In the name of Allah
# Ketchup CMS

Ketchup is a web application for content management your website. This application is based on laravel framework therefore it is easy to develop, and has appropriate security. Ketchup is open source and free, you can download it of this page and use forever.

## Some Features

Ketchup core functionality of a content management system to you offers. Features such as : 

1.Publish post

2.Create page

3.Upload/download and management files

4.User Management

5.Access Management

6.Create unlimited categories and tags

7.Receive comments and answer to them

... plus many other features.

## Official Documentation

Ketchup documents can be found here [Ketchup homepage](https://gitlab.com/alia_mehr/Ketchup/wikis).

## Contributing

For contributing & develop Ketchup CMS see here [Ketchup repository](https://gitlab.com/alia_mehr/Ketchup/tree/master).

## Issues

If the specific problem you encounter defined here [Ketchup issues](https://gitlab.com/alia_mehr/Ketchup/issues).

## License

The Ketchup CMS is open-sourced web application licensed under the [Apache License 2.0](http://www.apache.org/licenses/).
