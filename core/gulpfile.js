var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.styles([
    	'font-awesome.min.css',
    	'latofontstyle.css',
    	'bootstrap.min.css'
    ],'public/css/style.css');
    mix.scripts([
        'jquery.min.js',
        'bootstrap.min.js'
    ],'public/js/script.js');
});

elixir(function(mix) {
    mix.styles([
        'buttons.css',
        'calendar.css',
        'forms.css',
        'stats.css',
        'styles.css'
    ],'public/css/admin_style.css','resources/assets/panel/css');
    mix.scripts([
        'calendar.js',
        'editors.js',
        'forms.js',
        'stats.js',
        'tables.js',
        'custom.js'
    ],'public/js/admin_script.js','resources/assets/panel/js');
});

elixir(function(mix) {
    mix.version(['public/css/style.css','public/js/script.js','public/css/admin_style.css','public/js/admin_script.js']);
    //  mix.copy('public/build', '../build');
});