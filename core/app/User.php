<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'rule_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    function posts(){
        return $this->hasMany('App\Post');
    }

    public function access(){
        return $this->belongsTo('App\Access', 'rule_id');
    }

    public function information(){
        return $this->hasOne('App\Information');
    }

    public function files(){
        return $this->hasMany('App\File');
    }
}
