<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    function getSuperAdminAttribute(){
        return $this->user_id;
    }
}
