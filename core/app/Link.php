<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $dates = ['created_at', 'updated_at'];
}
