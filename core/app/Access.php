<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Access extends Model
{
    public function users(){
        return $this->hasMany('App\User', 'rule_id');
    }
}
