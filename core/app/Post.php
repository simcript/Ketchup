<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    protected $fillable = ['url', 'title', 'body', 'status', 'comments_status'];


    function user(){
    	return $this->belongsTo('App\User');
    }

    public function comments(){
        return $this->hasMany('App\Comment');
    }

    function tags(){
    	return $this->belongsToMany('App\Tag');
    }

    function cats(){
    	return $this->belongsToMany('App\Category');
    }

    function getCatIdsAttribute(){
        return $this->cats->lists('id')->toArray();
    }

    function getCatNamesAttribute(){
        return $this->cats->lists('name')->toArray();
    }

    function getTagNameAttribute(){
        return $this->tags->lists('name')->toArray();
    }

    function getAuthorNameAttribute(){
        return $this->user->name;
    }

}
