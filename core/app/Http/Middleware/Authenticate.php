<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\AccessController;
class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        }
        // if (!session()->has('menu')){
            $user = Auth::user();
            session([
                'mRule' => AccessController::allRules($user,'manage','tf'),
                'uRule' => AccessController::allRules($user,'user','tf'),
                'menu' => AccessController::menuAccess($user)
            ]);
        // }
        return $next($request);
    }
}
