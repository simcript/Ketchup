<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreatePostRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            'postTitle' => 'required|string|between:4,70',
            'postContent' => 'required|string|min:10',
            'postTags' => 'string',
            'category' => 'array|exists:categories,id'
        ];
    }
}
