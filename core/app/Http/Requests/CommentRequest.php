<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CommentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            'commName' => 'required|string|between:1,30',
            'commEmail' => 'required|email',
            'commBody' => 'required|string|min:10',
            'replyBody' => 'string|min:10',
            'statusComm' => 'required|integer|between:0,9',
            '_token' => 'required|alpha_dash'
        ];
    }
}
