<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'userName' => 'required|alpha_dash|max:255|unique:users,name',
            'userEmail' => 'required|email|unique:users,email',
            'userPass' => 'required|string|between:4,60',
            'userGroups' => 'required|integer|exists:accesses,id',

            'infoPicUrl' => 'active_url|between:4,940',
            'infoBrithDate' => 'date',
            'infoEmail' => 'email|between:4,220',
            'infoPhone' => 'numeric',
            'infoAddress' => 'string|between:4,300',
            'infoEducation' => 'string|between:4,180',
            'infoJob' => 'string|between:4,90',
            'infoFav' => 'string|between:4,450',
            'infoAbout' => 'string',
        ];
    }
}
