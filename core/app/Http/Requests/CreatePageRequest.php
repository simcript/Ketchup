<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreatePageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            'pageTitle' => 'required|string|between:4,70',
            'pageUrl' => 'required|alpha_dash|between:1,50',
            'pageContent' => 'required|string|min:10',
            'pageTags' => 'string'
        ];
    }
}
