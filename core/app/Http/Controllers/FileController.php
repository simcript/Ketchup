<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;

use Storage;
use Auth;
use App\User;
use App\File;
use Carbon\Carbon;
use App\Http\Controllers\SettingController;
use Illuminate\Http\Response;

class FileController extends Controller
{
    public function listFiles($from='me'){
        if ($from === 'me') {
            if ($this->uAr[16]) {
                $allFiles = Auth::user()->files()->get();
            }
        } else if ($from === 'all') {
            if ($this->mAr[24]) {
                $allFiles = File::get();
            }
        }
        return view('panel.files.index', compact('allFiles','from'));
    }

    public function editFile($id){
        if (!$this->uAr[18]) {
            return redirect('/panel/');
        } else if (!$this->mAr[26]) {
            return redirect('/panel/');
        }
        $eFile = File::withTrashed()->find($id);
        return view('panel.files.editfile', compact('eFile'));
    }

    public function upditeFile(Request $r, $id){
        if (!$this->uAr[18]) {
            return redirect('/panel/');
        } else if (!$this->mAr[26]) {
            return redirect('/panel/');
        }
        $this->validate($r,[
                'filePass' => 'string|between:4,20',
                'description' => 'string|max:450',
            ]);
        $eFile = File::withTrashed()->find($id);
        $eFile->password = strlen($r->filePass)<1 ? null : bcrypt($r->filePass);
        $eFile->description = $r->description;
        $eFile->save();
        return redirect('/panel/files/');
    }

    public function downloadFile($id){
        $dFile = File::find($id);
        $content = Storage::disk('local')->get($dFile->location);
        $dFile->totalDl++;
        $dFile->save();
        return (new Response($content, 200))->header('Content-Type', $dFile->mime);
    }

    public function deleteFile($id){
        $dFile = File::find($id);
        if ($dFile->user_id = Auth::user()->id) {
            if (!$this->uAr[17]) {
                return redirect('/panel/');
            }
        } else {
            if (!$this->mAr[25]) {
                return redirect('/panel/');
            }
        }
        $dFile->delete();
        return redirect('/panel/files/');
    }

    public function trashBin($from='me'){
        if ($from === 'me') {
            if ($this->uAr[16]) {
                $allFiles = Auth::user()->files()->onlyTrashed()->get();
            }
        } else if ($from === 'all') {
            if ($this->mAr[24]) {
                $allFiles = File::onlyTrashed()->get();
            }
        }
        $flag = true;
        return view('panel.files.index', compact('allFiles','from','flag'));
    }

    public function removeFile($id){
        $dFile = File::onlyTrashed()->find($id);
        if ($dFile->user_id = Auth::user()->id) {
            if (!$this->uAr[17]) {
                return redirect('/panel/');
            }
        } else {
            if (!$this->mAr[25]) {
                return redirect('/panel/');
            }
        }
        $dFile->forceDelete();
        Storage::delete($dFile->location);
        return redirect('/panel/files/trash');
    }

    public function restoreFile($id){
        $dFile = File::onlyTrashed()->find($id);
        if ($dFile->user_id = Auth::user()->id) {
            if (!$this->uAr[17]) {
                return redirect('/panel/');
            }
        } else {
            if (!$this->mAr[25]) {
                return redirect('/panel/');
            }
        }
        $dFile->restore();
        return redirect('/panel/files/trash');
    }

    public function newFile(Request $r){
    	if (!$this->uAr[15]) {
                return redirect('/panel/');
        }
    	if ($r->isMethod('POST')) {
            // set atribute
            $sett = SettingController::get();
            $this->validate($r,[
                'fileName' => 'alpha_dash|between:3,30|unique:files,name',
                'filePass' => 'string|between:4,20',
                'description' => 'string|max:450',
                'upFile' =>"required|file|mimes:$sett->mime|max:$sett->upsize"        // size & mime are of Settings Controller 
            ]);
            if (trim(strlen($r->fileName)) == 0) {
                $r->fileName = $r->file('upFile')->getClientOriginalName();
            }
            if (strpos($r->fileName, '.') === false) {
                $r->fileName = $r->fileName . '.' . $r->file('upFile')->guessClientExtension();
            }
            $user = Auth::user();
            if (Storage::exists($user->id .'/'. $r->fileName)) {
                $i=1;
                do {
                    $temp = explode('.', $r->fileName)[0];
                    $temp = $temp . '_' . $i++ . '.' . $r->file('upFile')->guessClientExtension();
                } while (Storage::exists($user->id .'/'. $temp));
            }
            $r->fileName = $temp;
            // save file
            Storage::makeDirectory($user->id);
    		Storage::put($user->id .'/'. $r->fileName,file_get_contents($r->file('upFile')->getRealPath()));
            // create file fild in datebase
            $tempTime = Carbon::now();
            $newFile = new File;
            $newFile->user_id = $user->id;
            $newFile->name = $r->fileName;
            $newFile->password = bcrypt($r->filePass);
            $newFile->description = $r->description;
            $newFile->mime = $r->file('upFile')->getClientMimeType();
            $newFile->size = $r->file('upFile')->getSize();
            $newFile->totalDl = 0;
            $newFile->link = Storage::url($r->fileName);
            $newFile->location = $user->id .'/'. $r->fileName;
            $newFile->code = bin2hex(openssl_random_pseudo_bytes(25));
            $newFile->created_at = $tempTime;
            $newFile->updated_at = $tempTime;
            $newFile->save();
            return redirect('/panel/files/');

    	} else{
            return view('panel.files.newfile');
        }
    }
}
