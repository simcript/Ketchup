<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Link;
class ToolsController extends Controller
{
    public function links(Request $r){
    	if (!$this->mAr[27]) {
            return redirect('/panel');
        }
        $editUrl = null;
        if ($r->isMethod('POST')) {
            $this->validate($r,[
                'linkId' => 'integer|exists:links,id'
            ]);
            switch ($r->opration) {
                case 'add':
                    $this->linkOpration($r);
                break;
                case 'display':
                    $editUrl = Link::find($r->linkId);
                break;
                case 'delete':
                    Link::find($r->linkId)->delete();
                break;
                
                default:
                    return redirect ('/panel');
                break;
            }
        }
        $allLinks = Link::get();
    	return view('panel.tools.links',compact('allLinks','editUrl'));
    }
    protected function linkOpration(Request $r){
        $this->validate($r,[
            'linkTitle' => 'required|string|between:4,90',
            'linkAddress' => 'required|url|between:4,490',
            'linkDescription' => 'string|between:1,290'
        ]);
        if (strlen($r->linkId) > 0) {
            $itUrl = Link::find($r->linkId);
        } else {
            $itUrl = new Link;
        }
        $itUrl->title = $r->linkTitle;
        $itUrl->address = $r->linkAddress;
        $itUrl->description = $r->linkDescription;
        return $itUrl->save();
    }

}
