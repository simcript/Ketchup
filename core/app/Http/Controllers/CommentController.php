<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\CommentRequest;
use App\Comment;
use App\Post;
use Auth;
use Carbon\Carbon;
class CommentController extends Controller
{
    public function listComm($from, $listType='lump'){
        if ($from =='me') {
            if ($this->uAr[11]) {
                $allPost = Auth::user()->posts()->get();
            } else {
                return redirect('/panel/comments/all');
            }
        } else if ($from =='all') {
            if ($this->mAr[8]) {
                $allPost = Post::get();
            } else {
                return redirect('/panel/');
            }
        }
    	
        foreach ($allPost as $post) {
            if ($listType=='spam') {
                $tempComments = $post->comments()->onlyTrashed()->get()->toArray();
            } else {
                $tempComments = $post->comments()->get()->toArray();
            }
            foreach ($tempComments as $comment) {
                if ($listType == 'lump') {
                    if ($comment['parent'] == null) {                   // This means that the Parent Comments
                        $comment['status'] = trans('comment.controll.status.'. $comment['status']);
                        $allComm[$comment['updated_at'].' '.$comment['id']] = $comment;
                    }
                } else if ($listType == 'mote'){
                    $comment['status'] = trans('comment.controll.status.'. $comment['status']);
                    $allComm[$comment['updated_at'].' '.$comment['id']] = $comment;
                } else if ($listType == 'spam'){
                    $comment['status'] = trans('comment.controll.status.5');
                    $allComm[$comment['deleted_at'].' '.$comment['id']] = $comment;
                } else {
                    return redirect('/panel/comments/me');
                }
            }
        }
        if (empty($allComm)) {
            $allComm = [];
        } else {
            $allComm = array_sort_recursive($allComm);
        }
        return view('panel.comments.index',compact('allComm','listType', 'from'));
    }


    public function reply($commId, $list=''){
        if ($this->mAr[6]) {
            $comm = Comment::withTrashed()->find($commId);
        } else if ($this->uAr[9]) {
            $comm = Comment::withTrashed()->find($commId);
            $tempUserId = Post::withTrashed()->find($comm->post_id)->user_id;
            if($tempUserId != Auth::user()->id){
                return redirect('/panel/');
            }
        } else {
            return redirect('/panel/');
        }
        $replyList = Comment::withTrashed()->where('parent',$comm->id)->get();
        $countReply = count($replyList);
        if (($list === 'all') && ($countReply > 0)) {
            foreach ($replyList as $comment) {
                if ($list == 'all'){
                    $comment['status'] = trans('comment.controll.status.'. $comment['status']);
                    $allComm[$comment['updated_at'].' '.$comment['id']] = $comment;
                    $AllcommentStatus = 'replies';
                }
            }
            $allComm = array_sort_recursive($allComm);
            return view('panel.comments.index',compact('allComm','AllcommentStatus'));
        } else if(($list === 'all') && ($countReply < 1)) {
            $tempLink = '/panel/comments/reply/' . $comm->id;
            return redirect($tempLink);
        }
        // $comm->status = 4;
        // $comm->save();
        return view('panel.comments.reply',compact(['comm','countReply']));
    }

    public function commReply(CommentRequest $r, $commId){
        if ($this->mAr[6]) {
            $comm = Comment::withTrashed()->find($commId);
        } else if ($this->uAr[9]) {
            $comm = Comment::withTrashed()->find($commId);
            $tempUserId = Post::withTrashed()->find($comm->post_id)->user_id;
            if($tempUserId != Auth::user()->id){
                return redirect('/panel/');
            }
        } else {
            return redirect('/panel/');
        }
        $comm = Comment::withTrashed()->find($commId);
        $tempTime = Carbon::now();
        $tempStatusReply = empty(trim(strip_tags($r->replyBody)));
        if (!$tempStatusReply) {
            $user = Auth::user();
            $myReply = new Comment;
            $myReply->post_id = $comm->post_id;
            $myReply->name = $user->name;
            $myReply->email = $user->email;
            $myReply->body = $r->replyBody;
            $myReply->parent = $comm->id;
            $myReply->status = 1;
            $myReply->csrf = $r->_token;
            $myReply->created_at = $tempTime;
            $myReply->updated_at = $tempTime;
            $this->newComment($myReply);
        }
        $comm->updated_at = $tempTime;
        if ($r->statusComm != $comm->status) {
            if ($r->statusComm == 5) {
                $comm->status = $r->statusComm;
                $this->upComment($comm);
                $this->spams($comm);
            } else if ($comm->status == 5) {
                $comm->status = $r->statusComm;
                $this->upComment($comm);
                $this->undoSpam($comm->id);
            }
        } else if((!$tempStatusReply) && ($comm->status == 5)) {
            $comm->status = $r->statusComm;
            $this->upComment($comm);
            $this->spams($myReply);
        }
        return redirect('/panel/comments/me');
    }

    public function deleteComm($commId){
        if ($this->mAr[7]) {
            $comm = Comment::withTrashed()->find($commId);
        } else if ($this->uAr[10]) {
            $comm = Comment::withTrashed()->find($commId);
            $tempUserId = Post::withTrashed()->find($comm->post_id)->user_id;
            if($tempUserId != Auth::user()->id){
                return redirect('/panel/');
            }
        } else {
            return redirect('/panel/');
        }
        $familyComment = Comment::where('parent',$comm->id)->get();
        $familyComment[] = $comm;
        foreach ($familyComment as $comm) {
            $comm->forceDelete();
        }
        return redirect('panel/comments/me');
    }

    protected function upComment(Comment $comm){
        return $comm->save();
    }

    protected function newComment(Comment $comm){
        return $comm->save();
    }

    protected function spams(Comment $comm){   // soft delete
        if ($this->mAr[6] && $this->mAr[7]) {
            $familyComment = Comment::where('parent',$comm->id)->get();
        } else if ($this->uAr[9] && $this->uAr[10]) {
            $tempUserId = Post::withTrashed()->find($comm->post_id)->user_id;
            if($tempUserId != Auth::user()->id){
                return redirect('/panel/');
            }
            $familyComment = Comment::where('parent',$comm->id)->get();
        } else {
            return redirect('/panel/');
        }

        $familyComment[] = $comm;
        foreach ($familyComment as $comm) {
            $comm->delete();
        }
    }

    protected function undoSpam($commId){
        if ($this->mAr[6] && $this->mAr[7]) {
            $comm = Comment::withTrashed()->find($commId);
        } else if ($this->uAr[9] && $this->uAr[10]) {
            $comm = Comment::withTrashed()->find($commId);
            $tempUserId = Post::withTrashed()->find($comm->post_id)->user_id;
            if($tempUserId != Auth::user()->id){
                return redirect('/panel/');
            }
        } else {
            return redirect('/panel/');
        }
        
        $comm->restore();
        $familyComment = Comment::onlyTrashed()->where('parent',$comm->id)->get();
        foreach ($familyComment as $comm) {
            if ($comm->status != 5) {
                $comm->restore();
            }
        }
    }
}
