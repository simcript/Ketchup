<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;
use App\Http\Controllers\SettingController;

class PanelController extends Controller
{
    
    public function index(){
    	$username = Auth::user()->toarray()['name'];
    	$sett = SettingController::get();
        return view('panel.index',compact('username','sett'));
    }
}
