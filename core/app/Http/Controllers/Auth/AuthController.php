<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Information;
use Carbon\Carbon;
use App\Setting;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/panel';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|alpha_dash|max:255|unique:users,name',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $init = Setting::find(1);

        if ($init->register == 0) {
            return redirect('/');
        }
        $temp = User::create([
            'name' => $data['name'],
            'rule_id' => $init->default_rule,
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
        $tempTime = Carbon::now();
        $info = new Information;
        $info->user_id = User::where('email',$data['email'])->first()->id;
        $info->name = $data['name'];
        $info->created_at = $tempTime;
        $info->updated_at = $tempTime;
        $info->save();
        return $temp;
    }
}
