<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests;
use App\Http\Controllers\SettingController;

use Auth;
use App\User;
use App\Information;
use App\Access;
use App\Post;
use Carbon\Carbon;

class UserController extends Controller
{
    public function activeUser(){
    	if (!$this->mAr[14]) {
    		return redirect('/panel/');
    	}
    	$allUsers = User::get();
        return view('panel.users.index',compact('allUsers'));
    }

    public function inActiveUser(){
    	if (!$this->mAr[17] && !$this->mAr[14]) {
    		return redirect('/panel/');
    	}
    	$allUsers = User::onlyTrashed()->get();
    	$flag = true;
        return view('panel.users.index',compact('allUsers','flag'));
    }

    public function changeStatus($id){
    	if (!$this->mAr[14]) {
    		return redirect('/panel/');
    	}

    	$tempUser = User::withTrashed()->find($id);
    	if ($tempUser->deleted_at != null) {
    		$tempUser->deleted_at = null;
    	} else {
    		$tempUser->deleted_at = Carbon::now();
    	}
    	$tempUser->save();

    	if (strpos(url()->previous(), 'inactive')) {
    		return redirect('panel/users/inactive');
    	} else {
    		return redirect('panel/users/');
    	}
    }
    public function deleteUser($id){
    	if (!$this->mAr[16]) {
    		return redirect('/panel/');
    	}
    	$tempUser = User::onlyTrashed()->find($id);
    	$tempUser->forceDelete();
        return redirect('/panel/users/inactive/');
    }

    public function newUser(){
        if (!$this->mAr[13]) {
            return redirect('/panel/');
        }
        $groups = Access::select('id','name')->get();
        return view('panel.users.newuser',compact('groups'));
    }

    public function createUser(CreateUserRequest $r){
        if (!$this->mAr[13]) {
            return redirect('/panel/');
        }
        $newUser = new User;
        $newUser = $this->saveUser($r,$newUser);

        $user = User::where('email', $r->userEmail)->first();

        $userInfo = new Information;
        $userInfo = $this->saveInfo($r,$userInfo,$user->id);

        $user->info_id = Information::where('user_id', $user->id)->first()->id;
        $user->save();

        return redirect('/panel/users/');
    }

    public function editUser($id = null){
    	if ($id == null) {
    		$user = Auth::user();
    	} else {
            if (!$this->mAr[15]) {
                return redirect('/panel/');
            }	
    		$user = User::withTrashed()->find($id);
    	}
        if ($this->mAr[15]) {
            $groups = Access::select('id','name')->get();
        } else {
            $groups = Access::select('id','name')->where('id',$user->rule_id)->get();
        }
    	$info = $user->information;
        
        return view('panel.users.edituser',compact('user','info', 'groups'));
    }

    public function upUser(UpdateUserRequest $r, $id = null){
        if ($id == null) {
            $user = Auth::user();
        } else { 
            if (!$this->mAr[15]) {
                return redirect('/panel/');
            }   
            $user = User::withTrashed()->find($id);
        }

        if ($r->userEmail != $user->email) {
            $this->validate($r,[
                'userEmail' => 'unique:users,email'
            ]);
        }

        $this->saveUser($r,$user,'edit');
        $this->saveInfo($r,$user->information,$user->id,'edit');

        return redirect('/panel/users/');
    }

    protected function saveUser(Request $r, User $tempUser, $type = 'new'){
        $tempTime = Carbon::now();

        $tempUser->name = $r->userName;
        $tempUser->email = $r->userEmail;
        if (strlen($r->userPass) > 0) {
            $tempUser->password = bcrypt($r->userPass);
        }
        if ($this->mAr[21]) {
            $tempUser->rule_id = $r->userGroups;
        } else {
            if ($type === 'new') {
                $sett = SettingController::get();
                $tempUser->rule_id = $sett->default_rule;
            }
        }
        
        if ($type === 'new') {
            $tempUser->created_at = $tempTime;
        }
        $tempUser->updated_at = $tempTime;
        return $tempUser->save();
    }

    protected function saveInfo(Request $r, Information $tempInfo, $userid, $type = 'new'){
        $tempTime = Carbon::now();

        $tempInfo->picture = $r->infoPicUrl;
        $tempInfo->brithTime = $r->infoBrithDate;
        $tempInfo->email = $r->infoEmail;
        $tempInfo->phone = $r->infoPhone;
        $tempInfo->address = $r->infoAddress;
        $tempInfo->education = $r->infoEducation;
        $tempInfo->job = $r->infoJob;
        $tempInfo->favorites = $r->infoFav;
        $tempInfo->about = $r->infoAbout;
        if ($type === 'new') {
            $tempInfo->user_id = $userid;
            $tempInfo->created_at = $tempTime;
        }
        $tempInfo->updated_at = $tempTime;
        return $tempInfo->save();
    }
}
