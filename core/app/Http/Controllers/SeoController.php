<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\Post;
use App\User;
use App\Category;
use App\Tag;
use Carbon\Carbon;

class SeoController extends Controller
{
    public function allCats(){
    	$allCategories = Category::all()->toArray();
    	return view('panel.seo.categories',compact('allCategories'));
    }

    public function allTags(){
    	$allTags = Tag::all()->toArray();
    	return view('panel.seo.tags',compact('allTags'));
    }

    public function deleteSeo(Request $r, $seoType, $id){
        if(strpos($r->Server('HTTP_REFERER'),substr($seoType,0,3))){
            if($seoType === 'category'){
                return $this->_deleteCat($id);
            } else if($seoType === 'tag'){
                return $this->_deleteTag($id);
            } else {
                return redirect('/panel');
            }
        } else {
            return redirect('/');
        }
    }

    public function editSeo(Request $r, $seoType, $id){
        if ($r->isMethod('GET')) {
            if($seoType === 'category'){
                $seoEdit = Category::where('id',$id)->first();
                return view('panel.seo.editseo',compact('seoEdit','seoType'));
            } else if($seoType === 'tag'){
                $seoEdit = Tag::where('id',$id)->first();
                return view('panel.seo.editseo',compact('seoEdit','seoType'));
            } else {
                return redirect('/panel');
            }
        } else if($r->isMethod('POST')){
            $this->validate($r, [
                'name' => 'required|alpha_dash|max:70'
            ]);
            $tempInput = $r->all();
            $tempInput['id'] = $id;
            if($seoType === 'category'){
                return $this->_editCat($tempInput);
            } else if($seoType === 'tag'){
                return $this->_editTag($tempInput);
            } else {
                return redirect('/panel');
            }
        }
    }

    public function newSeo(Request $r){
        if($r->isMethod('GET')){
    	   return view('panel.seo.newseo');
        } else if($r->isMethod('POST')){
            if($r->input('seoType')==="category"){
                $this->validate($r, [
                    'name' => 'required|alpha_dash|unique:categories|max:70'
                ]);
                return $this->_saveCat($r->all());
            } else if(($r->input('seoType')==="tag")){
                $this->validate($r, [
                    'name' => 'required|alpha_dash|unique:tags|max:70'
                ]);
                return $this->_saveTag($r->all());
            }
        }
    }

    protected function _saveCat($frmInputs){
        $tempTime = Carbon::now();
        $newCat = new Category;
        $newCat->name = $frmInputs['name'];
        $newCat->description = $frmInputs['seoContent'];
        $newCat->created_at = $tempTime;
        $newCat->updated_at = $tempTime;
        $newCat->save();
        return redirect('/panel/seo/categories');
    }

    protected function _saveTag($frmInputs){
        $tempTime = Carbon::now();
        $newTag = new Tag;
        $newTag->name = $frmInputs['name'];
        $newTag->description = $frmInputs['seoContent'];
        $newTag->created_at = $tempTime;
        $newTag->updated_at = $tempTime;
        $newTag->save();
        return redirect('/panel/seo/tags');
    }

    protected function _editCat($frmInputs){
        $upCat = Category::find($frmInputs['id']);
        $upCat->name = $frmInputs['name'];
        $upCat->description = $frmInputs['seoContent'];
        $upCat->updated_at = Carbon::now();
        $upCat->save();
        return redirect('/panel/seo/categories');
    }

    protected function _editTag($frmInputs){
        $upTag = Tag::find($frmInputs['id']);
        $upTag->name = $frmInputs['name'];
        $upTag->description = $frmInputs['seoContent'];
        $upTag->updated_at = Carbon::now();
        $upTag->save();
        return redirect('/panel/seo/tags');
    }

    public function _deleteCat($id){
        if($id==1){
            return redirect('/panel/seo/categories');
        }
        $delCat = Category::find($id);
        foreach ($delCat->posts as $p) {
            $upPost = Auth::user()->posts()->find($p['id']);
            if(count($upPost->cats) == 1){
                $tempCat = [0 => '1'];
                $upPost->cats()->sync($tempCat);
            }
        }
        $delCat->delete();
        return redirect('/panel/seo/categories');
    }

    public function _deleteTag($id){
        $delTag = Tag::find($id);
        $delTag->delete();
        return redirect('/panel/seo/tags');
    }
}
