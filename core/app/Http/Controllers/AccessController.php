<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Access;
use Crypt;
use Carbon\Carbon;
class AccessController extends Controller
{
	public static function getRule(User $user, $ruleID, $ruleType = 'user'){
        $userAccess = $user->access;
        if ($ruleType == 'user') {
            return (explode('|', Crypt::decrypt($userAccess->usage_rules))[$ruleID] == 0) ? false : true ;
        } else if($ruleType == 'manage') {
            return (explode('|', Crypt::decrypt($userAccess->manage_rules))[$ruleID] == 0) ? false : true;
        }
    }
    public static function allRules(User $user, $ruleType = 'user',$returnType = 'zerOne'){
        $userAccess = $user->access;
        if ($ruleType == 'user') {
            $temp = explode('|', Crypt::decrypt($userAccess->usage_rules));
        } else if($ruleType == 'manage') {
            $temp = explode('|', Crypt::decrypt($userAccess->manage_rules));
        }
        if ($returnType != 'zerOne') {
            foreach ($temp as $key => $value) {
                if ($value == 1) {
                    $temp[$key] = true;
                } else {
                    $temp[$key] = false;
                }
            }
        }
        return $temp;
    }

    public static function isManager(User $user){ 
        $userAccess = $user->access;
        $tempRules = Crypt::decrypt($userAccess->manage_rules);
        $tempCount1=substr_count($tempRules,'1');
        if ($tempCount1 > 0) {
            if ((count(explode('|', $tempRules)) > 20)) {
                $tempCount0=substr_count($tempRules,'0');
                $tempCount_=substr_count($tempRules,'|');
                $tempCount = $tempCount0 + $tempCount1 + $tempCount_;
                if($tempCount == strlen($tempRules)){
                    return true;
                }
            }
        }
        return false;
    }

    public static function menuAccess(User $user, $case = 'Dashboard'){
        $mAccess = self::allRules($user,'manage'); 
        $uAccess = self::allRules($user,'user');
        $listStatus = [];
        switch ($case) {
            case 'Dashboard':
                $listStatus['Dashboard'][0] = true;
            // break;
            case 'Post':
                if (($uAccess[0] + $uAccess[1] + $uAccess[2] + $uAccess[3])>0) {
                    $listStatus['Post'][0] = true;
                } else if (($mAccess[0] + $mAccess[1] + $mAccess[2])>0) {
                    $listStatus['Post'][0] = true;
                } else {
                    $listStatus['Post'][0] = false; 
                }
                $listStatus['Post']['all'] = (($uAccess[3] == 1) || ($mAccess[2] == 1)) ? true : false;
                $listStatus['Post']['new'] = ($uAccess[0] == 1) ? true : false;
                $listStatus['Post']['bin'] = (($uAccess[1]+$uAccess[2]+$uAccess[3] == 3) || ($mAccess[0]+$mAccess[1]+$mAccess[2] == 3)) ? true : false;
            // break;
            case 'Page':
                if (($uAccess[4] + $uAccess[5] + $uAccess[6] + $uAccess[7])>0) {
                    $listStatus['Page'][0] = true;
                } else if (($mAccess[3] + $mAccess[4] + $mAccess[5])>0) {
                    $listStatus['Page'][0] = true;
                } else {
                    $listStatus['Page'][0] = false; 
                }
                $listStatus['Page']['all'] = (($uAccess[7] == 1) || ($mAccess[5] == 1)) ? true : false;
                $listStatus['Page']['new'] = ($uAccess[4] == 1) ? true : false;
                $listStatus['Page']['bin'] = (($uAccess[5]+$uAccess[6]+$uAccess[7] == 3) || ($mAccess[3]+$mAccess[4]+$mAccess[5] == 3)) ? true : false;
            // break;
            case 'Comments':
                if (($uAccess[9] + $uAccess[10] + $uAccess[11])>0) {
                    $listStatus['Comments'][0] = true;
                } else if (($mAccess[6] + $mAccess[7] + $mAccess[8])>0) {
                    $listStatus['Comments'] = true;
                } else {
                    $listStatus['Comments'][0] = false; 
                }
                $listStatus['Comments']['all'] = (($uAccess[11] == 1) || ($mAccess[8] == 1)) ? true : false;
                $listStatus['Comments']['spam'] = (($uAccess[9]+$uAccess[11] == 2) || ($mAccess[6]+$mAccess[8] == 2)) ? true : false;
            // break;
            case 'Seo':
                if (($uAccess[12] + $uAccess[13] + $uAccess[14])>0) {
                    $listStatus['Seo'][0] = true;
                } else if (($mAccess[9] + $mAccess[10] + $mAccess[11] + $mAccess[12])>0) {
                    $listStatus['Seo'][0] = true;
                } else {
                    $listStatus['Seo'][0] = false; 
                }
                $listStatus['Seo']['categories'] = (($uAccess[14] == 1) || ($mAccess[11] == 1)) ? true : false;
                $listStatus['Seo']['tags'] = (($uAccess[14] == 1) || ($mAccess[11] == 1)) ? true : false;
                $listStatus['Seo']['new'] = (($uAccess[12] == 1) || ($mAccess[9] == 1)) ? true : false;
            // break;
            case 'Users':
                if (($mAccess[13] + $mAccess[14] + $mAccess[15] + $mAccess[16] + $mAccess[17] + $mAccess[18])>0) {
                    $listStatus['Users'][0] = true;
                } else {
                    $listStatus['Users'][0] = false; 
                }
            // break;

            case 'Groups':
                if (($mAccess[19] + $mAccess[20] + $mAccess[21] + $mAccess[22])>0) {
                    $listStatus['Groups'][0] = true;
                    $listStatus['Groups']['all'] = ($mAccess[21] == 1) ? true : false;
                    $listStatus['Groups']['new'] = ($mAccess[19] == 1) ? true : false;
                } else {
                    $listStatus['Groups'][0] = false; 
                }
            // break;
            
            case 'FMS':
                if (($uAccess[15] + $uAccess[16] + $uAccess[17] + $uAccess[18])>0) {
                    $listStatus['FMS'][0] = true;
                } else if (($mAccess[23] + $mAccess[24] + $mAccess[25])>0) {
                    $listStatus['FMS'][0] = true;
                } else {
                    $listStatus['FMS'][0] = false; 
                }
                $listStatus['FMS']['all'] = (($uAccess[16] == 1) || ($mAccess[23] == 1)) ? true : false;
                $listStatus['FMS']['new'] = ($uAccess[15] == 1) ? true : false;
                $listStatus['FMS']['bin'] = (($uAccess[16]+$uAccess[17]+$uAccess[18] == 3) || ($mAccess[23]+$mAccess[24]+$mAccess[25] == 3)) ? true : false;
            // break;

            case 'Settings':
                if (($mAccess[26])>0) {
                    $listStatus['Settings'][0] = true;
                } else {
                    $listStatus['Settings'][0] = false; 
                }
            // break;
            case 'Tools':
                if (($mAccess[27])>0) {
                    $listStatus['Tools'][0] = true;
                } else {
                    $listStatus['Tools'][0] = false; 
                }
            break;
        }
        return $listStatus;
    }

    public function index(){
        if (!$this->mAr[21]) {
            return redirect('panel');
        }
        $groupList = Access::get();
        return view('panel.groups.index',compact('groupList'));
    }

    public function newGroups(Request $r){
        if (!$this->mAr[19]) {
            return redirect('panel');
        }
        if ($r->isMethod('GET')) {
            $fieldsRules['user'] = trans('group.forms.new.user_box_list');
            $fieldsRules['manage'] = trans('group.forms.new.manage_box_list');
            return view('panel.groups.newgroup',compact('fieldsRules'));
        } else if ($r->isMethod('POST')) {
            $newAccess = new Access;
            $this->validate($r, [
                'groupTitle' => 'required|alpha_dash|between:3,30'
            ]);
            $newAccess = $this->actionAccess($r,$newAccess);
            $newAccess->save();
            return redirect('/panel/groups');
        }
    }

    public function editGroups(Request $r, Access $rule){
        if (!$this->mAr[20]) {
            return redirect('panel');
        }
        if ($r->isMethod('GET')) {
            $rule->usage_rules = explode('|', Crypt::decrypt($rule->usage_rules));
            $rule->manage_rules = explode('|', Crypt::decrypt($rule->manage_rules));
            $fieldsRules['user'] = trans('group.forms.new.user_box_list');
            $fieldsRules['manage'] = trans('group.forms.new.manage_box_list');
            $listAccs = Access::select('id','name')->where('name','<>',$rule->name)->get();
            return view('panel.groups.editgroup',compact('fieldsRules','rule', 'listAccs'));
        } else if ($r->isMethod('POST')) {
            if ($r->opration == 'save') {
                $this->validate($r, [
                    'groupTitle' => 'required|alpha_dash|between:3,30'
                ]);
                $rule = $this->actionAccess($r,$rule,'update');
                $rule->save();
            } else if ($r->opration == 'transfer') {
                $this->validate($r, [
                    'transferTo' => 'required|integer|exists:accesses,id'
                ]);
                $this->transfer($rule,$r->transferTo);
            }
            return redirect('/panel/groups');
        }
    }

    public function deleteGroups(Access $rule){
        if (!$this->mAr[22]) {
            return redirect('panel');
        }
        $rule->delete();
        return redirect('/panel/groups');
    }

    protected function transfer(Access $from, $to){
        if (!$this->mAr[20]) {
            return redirect('panel');
        }
        $userArr = $from->users;
        foreach ($userArr as $key => $user) {
            $user->rule_id = $to;
            $user->save();
        }
    }

    protected function actionAccess(Request $r, Access $acc, $type = 'new'){
        $acc->name = $r->groupTitle;
        $tempTime = Carbon::now();
        if ($type == 'new') {
            $acc->created_at = $tempTime;
        }
        $acc->updated_at = $tempTime;

        $ruleTemp = '';
        foreach ($r->userRule as $key) {
            $ruleTemp .= $key . '|';
        }
        $acc->usage_rules = Crypt::encrypt($ruleTemp);
        $ruleTemp = '';
        foreach ($r->manageRule as $key) {
            $ruleTemp .= $key . '|';
        }
        $acc->manage_rules = Crypt::encrypt($ruleTemp);
        return $acc;
    }
}
