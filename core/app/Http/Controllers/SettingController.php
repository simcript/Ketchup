<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;
use App\Setting;
use App\Access;
use Carbon\Carbon;
class SettingController extends Controller
{
    public static function get(){
        return (Setting::find(1)==null) ? redirect('panel/settings') : Setting::find(1) ;
    }

	public function index(){
		if (!$this->mAr[27]) {
            return redirect('/panel/');
        }
        $sett = self::get();
        $groups = Access::select('id','name')->get();
		return view('panel.settings.index',compact('sett','groups'));
	}

    public function change(Request $r){
        if (!$this->mAr[27]) {
            return redirect('/panel/');
        }
    	$this->validate($r,[
    		'siteTitle' => 'string|between:1,29',
    		'siteName' => 'required|string|between:4,59',
    		'siteDesc' => 'string|between:10,500',
    		'siteTags' => 'string|between:3,255',
    		'siteAvail' => 'required|string|between:3,6',
    		'siteNumPost' => 'required|integer|min:1|max:250',
    		'siteAbstract' => 'required|integer|min:30',
    		'siteFooter' => 'required|string|between:1,250',
    		'siteRtL' => 'required|integer|between:0,1',
    		'siteNotic' => 'string|min:20',
    		'panelNotic' => 'string|min:20',
            'authRegister' => 'required|integer|between:0,1',
    		'defaultUserGroups' => 'required|integer|exists:accesses,id',
    		'fileMime' => 'required|string|min:3',
    		'fileMaxsize' => 'required|integer|min:0',
    	]);
    	$sett = Setting::find(1);
        $tempTime = Carbon::now();
    	if ($sett == null) {
    		$sett = new Setting;
            $sett->user_id = Auth::user()->id;
            $sett->created_at = $tempTime;
    	}
        $sett->title = $r->siteTitle;
        $sett->name = $r->siteName;
        $sett->description = $r->siteDesc;
        $sett->tags = $r->siteTags;
        $sett->availability = $r->siteAvail;
        $sett->total_post = $r->siteNumPost;
        $sett->abstracts = $r->siteAbstract;
        $sett->footer = $r->siteFooter;
        $sett->rtl = $r->siteRtL;
        $sett->site_notic = $r->siteNotic;
        $sett->panel_notic = $r->panelNotic;
        $sett->register = $r->authRegister;
        $sett->default_rule = $r->defaultUserGroups;
        $sett->mime = $r->fileMime;
        $sett->upsize = $r->fileMaxsize;
        $sett->updated_at = $tempTime;
        $sett->save();
        return redirect('panel/settings');
    }
}