<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Http\Requests\CreatePageRequest;
use App\Post;
use App\User;
use App\Tag;
use Auth;
class PageController extends Controller
{
    
    public function listPages($from = 'me'){
        if ($this->uAr[7] || $this->mAr[5]) {
            switch ($from) {
                case 'me':
                    if(!$this->uAr[7]){
                        return redirect('panel/posts/all');
                    }
                    $allPages = Auth::user()->posts()->where('url','<>',null)->get();
                break;
                
                case 'all':
                    if(!$this->mAr[5]){
                        return redirect('panel/posts');
                    }
                    $allPages = Post::where('url','<>',null)->get();
                break;
            }
        } else {
            return redirect('panel');
        }
        return view('panel.pages.index',compact('allPages','from'));
    }

    public function newPage(){
        if (!$this->uAr[4]) {
            return redirect('panel');
        }
	    return view('panel.pages.newpage'); 
    }

    public function store(Request $r){
        if (!$this->uAr[4]) {
            return redirect('panel');
        }
        
        $data['url'] = $r->input('pageUrl');                                //for page posts
        $data['title'] = $r->input('pageTitle');
        $data['body'] = $r->input('pageContent');
        $data['status'] = $r->input('statusPage');
        $data['comments_status'] = $r->input('statusComment');
        $newpage = Auth::user()->posts()->create($data);
        if (strlen(trim($r->input('pageTags'))) > 0) {
            $data['pageTags'] = $this->_setTags($r->input('pageTags')) ;
            if ($data['pageTags'] != null) {
                $newpage->tags()->sync($data['pageTags']);
            }
        }
        return redirect('panel/pages');
    }

    public function edit(Post $post){
        if($post->user_id == Auth::user()->id){
            if (!$this->uAr[5]) {
                return redirect('panel');
            }
        } else {
            if (!$this->mAr[3]) {
                return redirect('panel');
            }
        }
        if ($post->url != null) {
            if ($post['user_id'] === Auth::user()->id) {
                return view('panel.pages.editpage',compact(['post']));
            } else {
                return redirect('panel/pages');
            }
        } else {
            return redirect('panel/pages');
        }
    }

    public function pageUpdate(CreatePageRequest $r,$id){
        $upPage = Post::find($id);
        if($upPage->user_id == Auth::user()->id){
            if (!$this->uAr[5]) {
                return redirect('panel');
            }
        } else {
            if (!$this->mAr[3]) {
                return redirect('panel');
            }
        }
        $data['url'] = $r->input('pageUrl');                                //for page posts
        $data['title'] = $r->input('pageTitle');
        $data['body'] = $r->input('pageContent');
        $data['status'] = $r->input('statusPage');
        $data['comments_status'] = $r->input('statusComment');
        $upPage->update($data);
        if (strlen(trim($r->input('pageTags'))) > 0) {
            $data['pageTags'] = $this->_setTags($r->input('pageTags')) ;
            if ($data['pageTags'] != null) {
                $upPage->tags()->sync($data['pageTags']);
            }
        }
        return redirect('panel/pages');
    }

    public function deletePage(Post $post){   // soft delete
        if($post->user_id == Auth::user()->id){
            if (!$this->uAr[6]) {
                return redirect('panel');
            }
        } else {
            if (!$this->mAr[4]) {
                return redirect('panel');
            }
        }

        if ($post->url != null) {
            $post->delete();                    //
        }
        return redirect('panel/pages');
    }

    protected function _setTags($allTags){
        $tagsArray = (strpos($allTags, '+') != false) ? explode('+', $allTags) : [ 0 => $allTags];
        $tempTag = new Tag;
        $idTag = null;
        foreach ($tagsArray as $tag) {
            $tempIdTag = $tempTag->select('id')->where('name', $tag)->first();
            if ($tempIdTag != null) {
                if (($this->uAr[14]) || ($this->mAr[11])) {
                    $tempIdTag = $tempIdTag->toArray();
                } else {
                    continue;
                }
            } else {
                if (($this->uAr[12]) || ($this->mAr[9])) {
                    $tempIdTag = $tempTag->create(['name' => $tag])->toArray();
                } else {
                    continue;
                }
            }
            $idTag[] = $tempIdTag['id'];
        }
        return $idTag;
    }

    public function trashBin($from = 'me'){
        if (($this->uAr[6] && $this->uAr[7]) || ($this->mAr[4] && $this->mAr[5])) {
            switch ($from) {
                case 'me':
                    if(!($this->uAr[6] && $this->uAr[7])){
                        return redirect('/panel/posts/trash/all');
                    }
                    $allPages = Auth::user()->posts()->onlyTrashed()->where('url','<>',null)->get();
                break;
                
                case 'all':
                    if(!($this->mAr[4] && $this->mAr[5])){
                        return redirect('/panel/posts/trash/');
                    }
                    $allPages = Post::onlyTrashed()->where('url','<>',null)->get();
                break;
            }
        } else {
            return redirect('panel');
        }
        return view('panel.pages.trashbin',compact('allPages','from'));
    }

    public function restorPage($id){
        $resPage = Post::onlyTrashed()->where('url','<>',null)->find($id);

        if($resPage->user_id == Auth::user()->id){
            if (!$this->uAr[6]) {
                return redirect('panel');
            }
        } else {
            if (!$this->mAr[4]) {
                return redirect('panel');
            }
        }
        $resPage->restore();
        return redirect('panel/pages/trash');
    }

    public function removePage($id){
        $delPage = Post::onlyTrashed()->where('url','<>',null)->find($id);

        if($delPage->user_id == Auth::user()->id){
            if (!$this->uAr[6]) {
                return redirect('panel');
            }
        } else {
            if (!$this->mAr[4]) {
                return redirect('panel');
            }
        }
        $delPage->forceDelete();
        return redirect('panel/pages/trash');
    }
}
