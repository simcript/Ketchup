<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['auth']], function () {
		// Route for Index panel
	Route::get('panel', ['uses'=>'PanelController@index']);

		// Route for Post section
	Route::get('/panel/posts/new', ['uses'=>'PostController@newPost']);
	Route::post('/panel/posts/new', ['uses'=>'PostController@store']);
	Route::post('/panel/posts/edit/{id}', ['uses'=>'PostController@postUpdate']);
	Route::get('/panel/posts/edit/{post}', ['uses'=>'PostController@edit']);
	Route::get('/panel/posts/delete/{post}', ['uses'=>'PostController@deletePost']);
	Route::get('/panel/posts/trash/restore/{id}', ['uses'=>'PostController@restorPost']);
	Route::get('/panel/posts/trash/remove/{id}', ['uses'=>'PostController@removePost']);
	Route::get('/panel/posts/trash/{from?}', ['uses'=>'PostController@trashBin']);
	Route::get('panel/posts/{from?}', ['uses'=>'PostController@listPosts']);

		// Route for Page section
	Route::get('/panel/pages/new', ['uses'=>'PageController@newPage']);
	Route::post('/panel/pages/new', ['uses'=>'PageController@store']);
	Route::post('/panel/pages/edit/{id}', ['uses'=>'PageController@pageUpdate']);
	Route::get('/panel/pages/edit/{post}', ['uses'=>'PageController@edit']);
	Route::get('/panel/pages/delete/{post}', ['uses'=>'PageController@deletePage']);
	Route::get('/panel/pages/trash', ['uses'=>'PageController@trashBin']);
	Route::get('/panel/pages/trash/restore/{id}', ['uses'=>'PageController@restorPage']);
	Route::get('/panel/pages/trash/remove/{id}', ['uses'=>'PageController@removePage']);
	Route::get('panel/pages/{from?}', ['uses'=>'PageController@listPages']);

		// Route for Comment section
	Route::get('/panel/comments/reply/{commId}/{list?}', ['uses'=>'CommentController@reply']);
	Route::post('/panel/comments/reply/{commId}', ['uses'=>'CommentController@commReply']);
	Route::get('/panel/comments/delete/{commId}', ['uses'=>'CommentController@deleteComm']);
	Route::get('/panel/comments/{from}/{listType?}', ['uses'=>'CommentController@listComm']);

		// Route for Seo section
	Route::get('/panel/seo/categories', ['uses'=>'SeoController@allCats']);
	Route::get('/panel/seo/tags', ['uses'=>'SeoController@allTags']);
	Route::match(['get','post'],'/panel/seo/new', ['uses'=>'SeoController@newSeo']);
	Route::match(['get','post'],'/panel/seo/edit/{seoType}/{id}', ['uses'=>'SeoController@editSeo']);
	Route::match(['get','post'],'/panel/seo/delete/{seoType}/{id}', ['uses'=>'SeoController@deleteSeo']);

		// Route for Group section
	Route::get('/panel/groups', ['uses'=>'AccessController@index']);
	Route::match(['get','post'],'/panel/groups/new', ['uses'=>'AccessController@newGroups']);
	Route::match(['get','post'],'/panel/groups/edit/{rule}', ['uses'=>'AccessController@editGroups']);
	Route::get('/panel/groups/delete/{rule}', ['uses'=>'AccessController@deleteGroups']);

		// Route for User section
	Route::get('/panel/users', ['uses'=>'UserController@activeUser']);
	Route::get('/panel/users/inactive', ['uses'=>'UserController@inActiveUser']);
	Route::get('/panel/users/status/{id}', ['uses'=>'UserController@changeStatus']);
	Route::get('/panel/users/delete/{id}', ['uses'=>'UserController@deleteUser']);
	Route::get('/panel/users/edit/{id?}', ['uses'=>'UserController@editUser']);
	Route::post('/panel/users/edit/{id?}', ['uses'=>'UserController@upUser']);
	Route::get('/panel/users/new', ['uses'=>'UserController@newUser']);
	Route::post('/panel/users/new', ['uses'=>'UserController@createUser']);

		// Route for File section
	Route::match(['get','post'],'/panel/files/new', ['uses'=>'FileController@newFile']);
	Route::get('/panel/files/edit/{id}', ['uses'=>'FileController@editFile']);
	Route::post('/panel/files/edit/{id}', ['uses'=>'FileController@upditeFile']);
	Route::get('/panel/files/delete/{id}', ['uses'=>'FileController@deleteFile']);
	Route::get('/panel/files/trash/restore/{id}', ['uses'=>'FileController@restoreFile']);
	Route::get('/panel/files/trash/remove/{id}', ['uses'=>'FileController@removeFile']);
	Route::get('/panel/files/trash/{from?}', ['uses'=>'FileController@trashBin']);
	Route::get('/panel/files/{from?}', ['uses'=>'FileController@listFiles']);

	// Route for Settings
	Route::get('/panel/settings', ['uses'=>'SettingController@index']);
	Route::post('/panel/settings', ['uses'=>'SettingController@change']);
	
	// Route for Tools
	Route::match(['get','post'],'/panel/tools/links', ['uses'=>'ToolsController@links']);
	// Route::match(['get','post'],'admin/user', ['middleware' => 'isAdmin','uses'=>'UserController@index']);
});

// Route::get('test/{locale?}', function ($locale='en') {
//     App::setLocale($locale);
//     return view('welcome');
// });

Route::auth();
Route::get('/file/{id}', ['uses'=>'FileController@downloadFile']);

Route::group(['middleware' => ['mainPage']], function () {
	Route::get('/', ['uses'=>'HomeController@index']);
	Route::get('/filter/', ['uses'=>'HomeController@filterPosts']);
	Route::get('/post/{post}', ['uses'=>'HomeController@showPost']);
	Route::post('/post/{post}', ['uses'=>'HomeController@createComment']);
	Route::get('/author/{user}', ['uses'=>'HomeController@profile']);
	Route::post('/{url}', ['uses'=>'HomeController@createComment']);
	Route::get('/{url}', ['uses'=>'HomeController@showPage']);
});