<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

            <title>{{ $init->title }} | {{ $info->name }}</title>
            <meta name="description" content=" {{ $info->about }} " />
            <meta name="keywords" content=" {{ $init->tags }} " />
            <meta name="author" content=" {{ $info->name }} " />

            <meta http-equiv="Designer" content="Sublime Text">
            <meta name="Generator" content="AliA_MehR | alia_mehr@yahoo.com">
            <meta name="copyright" content="Built-in time 2017-03-04 11:27:50 | website : https://gitlab.com/alia_mehr" />
            <meta name="robots" content="index, follow" />

    <link rel="stylesheet" href="{{ elixir('css/style.css') }}" >
    <style>
        body{
            background-color: #f1f1f1;
        } 
        #mainpage{
            background-color: #ffffff;
            margin-bottom: 200px;
        }

        /* Set black background color, white text and some padding */
        footer {
            background-color: #555;
            color: white;
            padding: 15px;
        }

        /* On small screens, set height to 'auto' for sidenav and grid */
        @media screen and (max-width: 767px) {
            .sidenav {
                height: auto;
                padding: 15px;
            }
            .row.content {height: auto;} 
        }
    </style>
</head>
<body>
    <div class="container-fluid">
        <div class="row content">
            <div class="col-sm-3 sidenav">
                <a href="{{ url('/') }}" style="color:#000;text-decoration:none;"><h3>{{ $init->name }}</h3></a>

                <h5>{{ $init->description }}</h5><br>
                <img src="{{ $info->picture }}" alt="{{ $info->name }}" width="250" height="200"><br><br>
                <ul class="nav nav-pills nav-stacked">
                    <li>Name: {{ $info->name }}</li>
                    <li>Brith date: {{ $info->brithTime }}</li>
                    <li>Education: {{ $info->education }}</li>
                    <li>Job: {{ $info->job }}</li>
                    <li>E-Mail: {{ $info->email }}</li>
                    <li>Telphone: {{ $info->phone }}</li>
                    <li>Address: {{ $info->address }}</li>
                    <li>Favorites: {{ $info->favorites }}</li>
                    <li>About me: {{ $info->about }}</li>
                </ul>
            </div>

            <div id="mainpage" class="col-sm-9">
                <h4><small>{{ $init->site_notic or trans('home.index.notic_place') }}</small></h4>
                <hr>
                <?php $colorsArr[0] = 'success';$colorsArr[1] = 'info';$colorsArr[2] = 'warning';$colorsArr[3] = 'danger'; ?>
                @foreach($posts as $post)
                    <h2><a href="{{ url('/post/'.$post->id) }}" style="text-decoration:none;">{{ $post->title }}</a></h2>
                    <h5><span class="glyphicon glyphicon-user"></span> &shy;<a href="{{ url('/filter/?author='.$post->user->name) }}">{{ $post->user->name }}</a> &shy;  <span class="glyphicon glyphicon-time"></span> &shy;<a href="{{ url('/filter/?date='.$post->updated_at) }}">{{ $post->updated_at }}</a> &shy;  <span class="glyphicon glyphicon-folder-open"></span> &shy; 
                    @foreach($post->catNames as $cat) 
                        <a href="{{ url('/filter/?category='.$cat) }}">{{ $cat }}</a>,
                    @endforeach
                    </h5>
                    <h5></h5>
                    {{ substr($post->body,0,$init->abstracts) }}
                    <h5>
                    
                    @foreach($post->tagName as $key => $tag) 
                        <span class="label label-{{ $colorsArr[$key%4] }}"><a href="{{ url('/filter/?tag='.$tag) }}">{{ $tag }}</a></span>
                    @endforeach
                    <br><br></h5>
                @endforeach
                    <br><br>
                {{ $posts->links() }}
            </div>
        </div>
    </div>
        <footer class="container-fluid navbar-fixed-bottom">
            <p class="text-center">{{ $init->footer }}</p>
        </footer>
    <script src="{{ elixir('js/script.js') }}"></script>
</body>
</html>

