@extends('layouts.panel')

@section('sidbaractive_seo','current')
@section('sidbaractive_alltag','current')
@section('content_panel')
<div class="col-md-10">
    <div class="row">
        <div class="col-md-12">
            <div class="content-box-large">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                  <th>@lang('seo.table.tags.rownumber')</th>
                                  <th>@lang('seo.table.tags.title')</th>
                                  <th>@lang('seo.table.tags.content')</th>
                                  <th>@lang('seo.table.tags.date')</th>
                                  <th>@lang('seo.table.tags.oprations')</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($allTags as $key => $tag)
                                <tr>
                                  <td>{{ $key+1 }}</td>
                                  <td>{{ substr($tag['name'],0,20) }}</td>
                                  <td>{{ substr($tag['description'],0,60) }}</td>
                                  <td>{{ $tag['updated_at'] }}</td>
                                  <td><a href="{{ url('/panel/seo/edit/tag') .'/'. $tag['id'] }}"><i class="glyphicon glyphicon-edit"></i></a></td>
                                  <td><a href="{{ url('/panel/seo/delete/tag') .'/'. $tag['id'] }}"><i class="glyphicon glyphicon-remove"></i></a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection