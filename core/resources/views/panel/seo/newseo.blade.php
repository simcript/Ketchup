@extends('layouts.panel')

@section('sidbaractive_seo','current')
@section('sidbaractive_newseo','current')
@section('content_panel')
<link href="/build/tags/css/bootstrap-tags.css" rel="stylesheet">

<div class="col-md-10">
    <div class="row">
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/panel/seo/new') }}">
        <div class="col-md-9">
            <div class="content-box-header">
                <div class="panel-title">@lang('seo.forms.new.main_box_title')</div>

                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-repeat"></i></a>
                </div>
            </div>
            <div class="content-box-large box-with-header">
                
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <input id="name" type="text" class="form-control" name="name" placeholder="@lang('seo.forms.new.textbox_title')" value="{{ old('name') }}">
                        @if ($errors->has('name'))
                            <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('seoContent') ? ' has-error' : '' }}">
                        <textarea rows="20" id="tinymce_basic" name="seoContent">{{   empty(old('seoContent')) ? trans('seo.forms.new.textbox_content') : old('seoContent') }}</textarea>
                        @if ($errors->has('seoContent'))
                            <span class="help-block">
                            <strong>{{ $errors->first('seoContent') }}</strong>
                            </span>
                        @endif
                    </div>
            </div>
        
        </div>
        <div class="col-md-3">
            <div class="content-box-header">
                <div class="panel-title">@lang('seo.forms.new.save_box_title')</div>
            </div>
            <div class="content-box-large box-with-header">

                <button type="submit" class="btn btn-success btn-lg btn-block" name="seoType" value="category"><i class="glyphicon glyphicon-th-list"></i> @lang('seo.forms.new.btn_save_cat') </button>

                <button type="submit" class="btn btn-success btn-lg btn-block" name="seoType" value="tag"><i class="glyphicon glyphicon-tag"></i> @lang('seo.forms.new.btn_save_tag') </button>

            </div>
        </div>
    </form>

    </div>
</div>


    <script type="text/javascript" src="/build/tinymce/js/tinymce/tinymce.min.js"></script>
    <script src="/build/css/js/editors.js"></script>


@endsection