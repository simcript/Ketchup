@extends('layouts.panel')

@section('sidbaractive_seo','current')
@section('sidbaractive_allcat','current')
@section('content_panel')
<div class="col-md-10">
    <div class="row">
        <div class="col-md-12">
            <div class="content-box-large">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                  <th>@lang('seo.table.cats.rownumber')</th>
                                  <th>@lang('seo.table.cats.title')</th>
                                  <th>@lang('seo.table.cats.content')</th>
                                  <th>@lang('seo.table.cats.date')</th>
                                  <th>@lang('seo.table.cats.oprations')</th>
                              </tr>
                          </thead>
                          <tbody>
                            @foreach($allCategories as $key => $cat)
                            <tr>
                              <td>{{ $key+1 }}</td>
                              <td>{{ substr($cat['name'],0,20) }}</td>
                              <td>{{ substr($cat['description'],0,60) }}</td>
                              <td>{{ $cat['updated_at'] }}</td>
                              <td><a href="{{ url('/panel/seo/edit/category') .'/'. $cat['id'] }}"><i class="glyphicon glyphicon-edit"></i></a></td>
                              @if($cat['id']>1)
                                <td><a href="{{ url('/panel/seo/delete/category') .'/'. $cat['id'] }}"><i class="glyphicon glyphicon-remove"></i></a></td>
                              @endif
                          </tr>
                          @endforeach
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
  </div>
</div>
</div>
@endsection