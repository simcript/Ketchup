@extends('layouts.panel')

@section('sidbaractive_file','current')
@if(isset($flag))
  @section('sidbaractive_trashfile','current')
@else
  @section('sidbaractive_allfile','current')
@endif
@section('content_panel')
<div class="col-md-10">
    <div class="row">
        <div class="col-md-12">
            <div class="content-box-large">
              <div class="panel-heading">
                    <div class="panel-title"></div>
                    
                    <div class="panel-options">
                    <?php if (isset($from)) { ?>
                      <div class="btn-group btn-group-xs" role="group" aria-label="...">
                        <a class="btn btn-default<?= ($from == 'me') ? '-selected': ''; ?>" href="{{ url('/panel/files') }}">@lang('panel.adminOption.me')</a>
                        <a class="btn btn-default<?= ($from == 'all') ? '-selected': ''; ?>" href="{{ url('/panel/files/all') }}">@lang('panel.adminOption.all')</a>
                      </div>
                    <?php } ?>

                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                  <th>@lang('file.table.rownumber')</th>
                                  <th>@lang('file.table.title')</th>
                                  <th>@lang('file.table.mime')</th>
                                  <th>@lang('file.table.totalDl')</th>
                                  <th>@lang('file.table.uptime')</th>
                                  <th>@lang('file.table.oprations')</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($allFiles as $key => $file)
                                <tr>
                                  <td>{{ $key+1 }}</td>
                                  <td>{{ $file['name'] }}</td>
                                  <td>{{ $file['mime'] }}</td>
                                  <td>{{ $file['totalDl'] }}</td>
                                  <td>{{ $file['created_at'] }}</td>

                                  @if(session('uRule')[18] || session('mRule')[26])
                                    <td><a href="{{ url('/panel/files/edit') .'/'. $file['id'] }}"><i class="glyphicon glyphicon-edit"></i></a></td>
                                  @endif
                                  @if(session('uRule')[16] || session('mRule')[24])
                                    @if(!isset($flag))
                                      <td><a href="{{ url('/file/') .'/'. $file['id'] }}"><i class="glyphicon glyphicon-download"></i></a></td>
                                    @endif
                                  @endif
                                  @if(session('uRule')[17] || session('mRule')[25])
                                    @if(isset($flag))
                                      <td><a href="{{ url('/panel/files/trash/restore') .'/'. $file['id'] }}"><i class="glyphicon glyphicon-share-alt"></i></a></td>
                                      <td><a href="{{ url('/panel/files/trash/remove') .'/'. $file['id'] }}"><i class="glyphicon glyphicon-remove"></i></a></td>
                                    @else
                                      <td><a href="{{ url('/panel/files/delete') .'/'. $file['id'] }}"><i class="glyphicon glyphicon-trash"></i></a></td>
                                    @endif
                                  @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection