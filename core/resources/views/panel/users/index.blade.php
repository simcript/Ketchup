@extends('layouts.panel')
@section('sidbaractive_user','current')

@if (isset($flag))
  @section('sidbaractive_inactuser','current')
@else
  @section('sidbaractive_actuser','current')
@endif

@section('content_panel')
<div class="col-md-10">
    <div class="row">
        <div class="col-md-12">
            <div class="content-box-large">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                  <th>@lang('user.table.rownumber')</th>
                                  <th>@lang('user.table.name')</th>
                                  <th>@lang('user.table.email')</th>
                                  <th>@lang('user.table.joined')</th>
                                  <th>@lang('user.table.oprations')</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($allUsers as $key => $user)
                                <tr>
                                  <td>{{ $key+1 }}</td>
                                  <td>{{ $user['name'] }}</td>
                                  <td>{{ $user['email'] }}</td>
                                  <td>{{ $user['created_at'] }}</td>
                                  @if(session('mRule')[15])
                                    <td><a href="{{ url('/panel/users/edit') .'/'. $user['id'] }}"><i class="glyphicon glyphicon-edit"></i></a></td>
                                  @endif
                                  @if(session('mRule')[17])
                                    <td><a href="{{ url('/panel/users/status') . '/'. $user['id'] }}"><i class="glyphicon glyphicon-adjust"></i></a></td>
                                  @endif
                                  @if(isset($flag) && session('mRule')[16])
                                    <td><a href="{{ url('/panel/users/delete') .'/'. $user['id'] }}"><i class="glyphicon glyphicon-remove"></i></a></td>
                                  @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection