@extends('layouts.panel')

@section('sidbaractive_user','current')
@section('sidbaractive_newuser','current')
@section('content_panel')
<?php
// dd($errors); 
?>
<link href="/build/tags/css/bootstrap-tags.css" rel="stylesheet">

<div class="col-md-10">
    <div class="row">
    <form class="form-horizontal" role="form" method="POST" action="">
        <div class="col-md-4">
            <div class="content-box-header">
                <div class="panel-title">@lang('user.forms.new.account_box_title')</div>

                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-repeat"></i></a>
                </div>
            </div>
            <div class="content-box-large box-with-header">
                
                    {{ csrf_field() }}


                    <div class="form-group{{ $errors->has('userName') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <div class="input-group-addon">@lang('user.forms.new.textbox_username')</div>
                            <input id="userName" type="text" class="form-control" name="userName" value="{{ old('userName') }}">
                            
                        </div>
                        @if ($errors->has('userName'))
                            <span class="help-block">
                            <strong>{{ $errors->first('userName') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('userEmail') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <div class="input-group-addon">@lang('user.forms.new.textbox_email')</div>
                            <input id="userEmail" type="email" class="form-control" name="userEmail" value="{{ old('userEmail') }}">
                            
                        </div>
                        @if ($errors->has('userEmail'))
                            <span class="help-block">
                            <strong>{{ $errors->first('userEmail') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('userPass') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <div class="input-group-addon">@lang('user.forms.new.textbox_pass')</div>
                            <input id="userPass" type="password" class="form-control" name="userPass" value="{{ old('userPass') }}">
                        </div>
                        @if ($errors->has('userPass'))
                            <span class="help-block">
                            <strong>{{ $errors->first('userPass') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('userGroups') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <div class="input-group-addon">@lang('user.forms.new.selectbox_groups')</div>
                            <select name="userGroups" id="select-userGroups" class="form-control custom-scroll">
                                <option value="0">@lang('user.forms.new.selectbox_default')</option>
                                @foreach($groups as $g)
                                    <option value="{{ $g->id }}">{{ $g->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        @if ($errors->has('userGroups'))
                            <span class="help-block">
                            <strong>{{ $errors->first('userGroups') }}</strong>
                            </span>
                        @endif
                    </div>
            </div>
        </div>

        <div class="col-md-5">
            <div class="content-box-header">
                <div class="panel-title">@lang('user.forms.new.profile_box_title')</div>

                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-repeat"></i></a>
                </div>
            </div>
            <div class="content-box-large box-with-header">

                <div class="form-group{{ $errors->has('infoName') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-addon">@lang('user.forms.new.textbox_name')</div>
                        <input id="infoName" type="text" class="form-control" name="infoName" value="{{ old('infoName') }}">
                    </div>
                    @if ($errors->has('infoName'))
                        <span class="help-block">
                        <strong>{{ $errors->first('infoName') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('infoPicUrl') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-addon">@lang('user.forms.new.textbox_pic')</div>
                        <input id="infoPicUrl" type="url" class="form-control" name="infoPicUrl" value="{{ old('infoPicUrl') }}">
                    </div>
                    @if ($errors->has('infoPicUrl'))
                        <span class="help-block">
                        <strong>{{ $errors->first('infoPicUrl') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('infoBrithDate') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-addon">@lang('user.forms.new.textbox_bd')</div>
                        <input id="infoBrithDate" type="date" class="form-control" name="infoBrithDate" placeholder="@lang('user.forms.new.textbox_name')" value="{{ old('infoBrithDate') }}">
                    </div>
                    @if ($errors->has('infoBrithDate'))
                        <span class="help-block">
                        <strong>{{ $errors->first('infoBrithDate') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('infoEmail') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-addon">@lang('user.forms.new.textbox_infomail')</div>
                        <input id="infoEmail" type="email" class="form-control" name="infoEmail" value="{{ old('infoEmail') }}">
                    </div>
                    @if ($errors->has('infoEmail'))
                        <span class="help-block">
                        <strong>{{ $errors->first('infoEmail') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('infoPhone') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-addon">@lang('user.forms.new.textbox_phone')</div>
                        <input id="infoPhone" type="text" class="form-control" name="infoPhone" value="{{ old('infoPhone') }}">
                    </div>
                    @if ($errors->has('infoPhone'))
                        <span class="help-block">
                        <strong>{{ $errors->first('infoPhone') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('infoAddress') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-addon">@lang('user.forms.new.textbox_address')</div>
                        <input id="infoAddress" type="text" class="form-control" name="infoAddress" value="{{ old('infoAddress') }}">
                    </div>
                    @if ($errors->has('infoAddress'))
                        <span class="help-block">
                        <strong>{{ $errors->first('infoAddress') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('infoEducation') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-addon">@lang('user.forms.new.textbox_education')</div>
                        <input id="infoEducation" type="text" class="form-control" name="infoEducation" value="{{ old('infoEducation') }}">
                    </div>
                    @if ($errors->has('infoEducation'))
                        <span class="help-block">
                        <strong>{{ $errors->first('infoEducation') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('infoJob') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-addon">@lang('user.forms.new.textbox_job')</div>
                        <input id="infoJob" type="text" class="form-control" name="infoJob" value="{{ old('infoJob') }}">
                    </div>
                    @if ($errors->has('infoJob'))
                        <span class="help-block">
                        <strong>{{ $errors->first('infoJob') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('infoFav') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-addon">@lang('user.forms.new.textbox_favorites')</div>
                        <textarea class="form-control" rows="3" id="infoFav" name="infoFav">{{ old('infoFav') }}</textarea>
                    </div>
                    @if ($errors->has('infoFav'))
                        <span class="help-block">
                        <strong>{{ $errors->first('infoFav') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('infoAbout') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-addon">@lang('user.forms.new.textbox_about')</div>
                        <textarea class="form-control" rows="7" id="infoAbout" name="infoAbout">{{ old('infoAbout') }}</textarea>
                    </div>
                    @if ($errors->has('infoAbout'))
                        <span class="help-block">
                        <strong>{{ $errors->first('infoAbout') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>


        <div class="col-md-3">
            <div class="content-box-header">
                <div class="panel-title">@lang('user.forms.new.save_box_title')</div>
            </div>
            <div class="content-box-large box-with-header">
                
                <button type="submit" class="btn btn-success btn-lg btn-block" name="opration" value="Add"><i class="glyphicon glyphicon-saved"></i> @lang('user.forms.new.btn_send') </button>
            </div>
        </div>
    </form>

    </div>
</div>


@endsection

