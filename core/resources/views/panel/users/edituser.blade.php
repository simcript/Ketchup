@extends('layouts.panel')

@section('sidbaractive_user','current')
@section('sidbaractive_edituser','current')
@section('content_panel')
<link href="/build/tags/css/bootstrap-tags.css" rel="stylesheet">

<div class="col-md-10">
    <div class="row">
    <form class="form-horizontal" role="form" method="POST" action="">
        <div class="col-md-4">
            <div class="content-box-header">
                <div class="panel-title">@lang('user.forms.edit.account_box_title',['username'=>$user->name])</div>

                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-repeat"></i></a>
                </div>
            </div>
            <div class="content-box-large box-with-header">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('userName') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <div class="input-group-addon">@lang('user.forms.new.textbox_username')</div>
                            <input id="userName" type="text" class="form-control" name="userName" value="{{ $user->name }}">
                        </div>
                        @if ($errors->has('userName'))
                            <span class="help-block">
                            <strong>{{ $errors->first('userName') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('userEmail') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <div class="input-group-addon">@lang('user.forms.new.textbox_email')</div>
                            <input id="userEmail" type="email" class="form-control" name="userEmail" value="{{ $user->email }}">
                            
                        </div>
                        @if ($errors->has('userEmail'))
                            <span class="help-block">
                            <strong>{{ $errors->first('userEmail') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('userPass') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <div class="input-group-addon">@lang('user.forms.new.textbox_pass')</div>
                            <input id="userPass" type="password" class="form-control" placeholder="@lang('user.forms.edit.textbox_pass')" name="userPass">
                        </div>
                        @if ($errors->has('userPass'))
                            <span class="help-block">
                            <strong>{{ $errors->first('userPass') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('userGroups') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <div class="input-group-addon">@lang('user.forms.new.selectbox_groups')</div>
                            <select name="userGroups" id="select-userGroups" class="form-control custom-scroll">
                                <option value="0">@lang('user.forms.new.selectbox_default')</option>
                                @foreach($groups as $g)
                                    <option @if ($user->rule_id == $g->id ) selected @endif value="{{ $g->id }}">{{ $g->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        @if ($errors->has('userGroups'))
                            <span class="help-block">
                            <strong>{{ $errors->first('userGroups') }}</strong>
                            </span>
                        @endif
                    </div>
            </div>
        </div>

        <div class="col-md-5">
            <div class="content-box-header">
                <div class="panel-title">@lang('user.forms.edit.profile_box_title',['username'=>$user->name])</div>

                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-repeat"></i></a>
                </div>
            </div>
            <div class="content-box-large box-with-header">
                <div class="form-group{{ $errors->has('infoName') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-addon">@lang('user.forms.new.textbox_name')</div>
                        <input id="infoName" type="text" class="form-control" name="infoName" value="{{ $info->name }}">
                    </div>
                    @if ($errors->has('infoName'))
                        <span class="help-block">
                        <strong>{{ $errors->first('infoName') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('infoPicUrl') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-addon">@lang('user.forms.new.textbox_pic')</div>
                        <input id="infoPicUrl" type="url" class="form-control" name="infoPicUrl" value="{{ $info->picture }}">
                    </div>
                    @if ($errors->has('infoPicUrl'))
                        <span class="help-block">
                        <strong>{{ $errors->first('infoPicUrl') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('infoBrithDate') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-addon">@lang('user.forms.new.textbox_bd')</div>
                        <input id="infoBrithDate" type="date" class="form-control" name="infoBrithDate" placeholder="@lang('user.forms.new.textbox_name')" value="{{ $info->brithTime }}">
                    </div>
                    @if ($errors->has('infoBrithDate'))
                        <span class="help-block">
                        <strong>{{ $errors->first('infoBrithDate') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('infoEmail') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-addon">@lang('user.forms.new.textbox_infomail')</div>
                        <input id="infoEmail" type="email" class="form-control" name="infoEmail" value="{{ $info->email }}">
                    </div>
                    @if ($errors->has('infoEmail'))
                        <span class="help-block">
                        <strong>{{ $errors->first('infoEmail') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('infoPhone') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-addon">@lang('user.forms.new.textbox_phone')</div>
                        <input id="infoPhone" type="text" class="form-control" name="infoPhone" value="{{ $info->phone }}">
                    </div>
                    @if ($errors->has('infoPhone'))
                        <span class="help-block">
                        <strong>{{ $errors->first('infoPhone') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('infoAddress') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-addon">@lang('user.forms.new.textbox_address')</div>
                        <input id="infoAddress" type="text" class="form-control" name="infoAddress" value="{{ $info->address }}">
                    </div>
                    @if ($errors->has('infoAddress'))
                        <span class="help-block">
                        <strong>{{ $errors->first('infoAddress') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('infoEducation') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-addon">@lang('user.forms.new.textbox_education')</div>
                        <input id="infoEducation" type="text" class="form-control" name="infoEducation" value="{{ $info->education }}">
                    </div>
                    @if ($errors->has('infoEducation'))
                        <span class="help-block">
                        <strong>{{ $errors->first('infoEducation') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('infoJob') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-addon">@lang('user.forms.new.textbox_job')</div>
                        <input id="infoJob" type="text" class="form-control" name="infoJob" value="{{ $info->job }}">
                    </div>
                    @if ($errors->has('infoJob'))
                        <span class="help-block">
                        <strong>{{ $errors->first('infoJob') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('infoFav') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-addon">@lang('user.forms.new.textbox_favorites')</div>
                        <textarea class="form-control" rows="3" id="infoFav" name="infoFav">{{ $info->favorites }}</textarea>
                    </div>
                    @if ($errors->has('infoFav'))
                        <span class="help-block">
                        <strong>{{ $errors->first('infoFav') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('infoAbout') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-addon">@lang('user.forms.new.textbox_about')</div>
                        <textarea class="form-control" rows="7" id="infoAbout" name="infoAbout">{{ $info->about }}</textarea>
                    </div>
                    @if ($errors->has('infoAbout'))
                        <span class="help-block">
                        <strong>{{ $errors->first('infoAbout') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>


        <div class="col-md-3">
            <div class="content-box-header">
                <div class="panel-title">@lang('user.forms.edit.save_box_title')</div>
            </div>
            <div class="content-box-large box-with-header">
                <p>@lang('user.forms.edit.save_box_info',['username'=>$user->name, 'create_time'=>$user->created_at, 'up_time'=>$user->updated_at])</p>
                <button type="submit" class="btn btn-success btn-lg btn-block" name="opration" value="Add"><i class="glyphicon glyphicon-saved"></i> @lang('user.forms.new.btn_send') </button>
            </div>
        </div>
    </form>

    </div>
</div>


@endsection

