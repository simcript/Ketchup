@extends('layouts.panel')

@section('sidbaractive_groups','current')
@section('sidbaractive_allgroups','current')
@section('content_panel')
<div class="col-md-10">
    <div class="row">
        <div class="col-md-12">
            <div class="content-box-large">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                  <th>@lang('group.table.rownumber')</th>
                                  <th>@lang('group.table.title')</th>
                                  <th>@lang('group.table.date')</th>
                                  <th>@lang('group.table.update')</th>
                                  <th>@lang('group.table.oprations')</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($groupList as $key => $group)
                                <tr>
                                  <td>{{ $key+1 }}</td>
                                  <td>{{ $group['name'] }}</td>
                                  <td>{{ $group['created_at'] }}</td>
                                  <td>{{ $group['updated_at'] }}</td>
                                  <td><a href="{{ url('/panel/groups/edit') .'/'. $group['id'] }}"><i class="glyphicon glyphicon-edit"></i></a></td>
                                  <td><a href="{{ url('/panel/groups/delete') .'/'. $group['id'] }}"><i class="glyphicon glyphicon-trash"></i></a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection