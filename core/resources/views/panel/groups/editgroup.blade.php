@extends('layouts.panel')

@section('sidbaractive_groups','current')
@section('content_panel')
<link href="/build/tags/css/bootstrap-tags.css" rel="stylesheet">

<div class="col-md-10">
    <div class="row">
    <form class="form-horizontal" role="form" method="POST" action="">
        <div class="col-md-5">
            <div class="content-box-header">
                <div class="panel-title">@lang('group.forms.edit.user_box_title')</div>

                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-repeat"></i></a>
                </div>
            </div>
            <div class="content-box-large box-with-header">
                
                @foreach ($fieldsRules['user'] as $key => $ruleText)
                    <h4>
                        <span class="label label-default">{{ $ruleText }}</span>
                        <div class="btn-group pull-right" data-toggle="buttons">
                            <label class="btn btn-default btn-xs @if($rule->usage_rules[$key]) active @endif">
                                <input type="radio" name="userRule[{{ $key }}]" id="option{{ $key }}" value="1" @if($rule->usage_rules[$key]) checked @endif > @lang('group.forms.new.btn_allow')
                            </label>
                            <label class="btn btn-default btn-xs @if(!$rule->usage_rules[$key]) active @endif">
                                <input type="radio" name="userRule[{{ $key }}]" id="option{{ $key }}" value="0" @if(!$rule->usage_rules[$key]) checked @endif> @lang('group.forms.new.btn_ban')
                            </label>
                        </div>
                    </h4>
                @endforeach
                
            </div>
        </div>
        <div class="col-md-5">
            <div class="content-box-header">
                <div class="panel-title">@lang('group.forms.edit.manage_box_title')</div>

                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-repeat"></i></a>
                </div>
            </div>
            <div class="content-box-large box-with-header">

                @foreach ($fieldsRules['manage'] as $key => $ruleText)
                    <h4>
                        <span class="label label-default">{{ $ruleText }}</span>
                        <div class="btn-group pull-right" data-toggle="buttons">
                            <label class="btn btn-default btn-xs @if($rule->manage_rules[$key]) active @endif" >
                                <input type="radio" name="manageRule[{{ $key }}]" id="option{{ $key }}" value="1" @if($rule->manage_rules[$key]) checked @endif> @lang('group.forms.new.btn_allow')
                            </label>
                            <label class="btn btn-default btn-xs @if(!$rule->manage_rules[$key]) active @endif">
                                <input type="radio" name="manageRule[{{ $key }}]" id="option{{ $key }}" value="0" @if(!$rule->manage_rules[$key]) checked @endif > @lang('group.forms.new.btn_ban')
                            </label>
                        </div>
                    </h4>
                @endforeach
            </div>
        </div>

        <div class="col-md-2">
            <div class="content-box-header">
                <div class="panel-title">@lang('group.forms.edit.save_box_title')</div>
            </div>
            <div class="content-box-large box-with-header">
                <div class="form-group{{ $errors->has('groupTitle') ? ' has-error' : '' }}">
                    <input id="groupTitle" type="text" class="form-control" name="groupTitle" value="{{ $rule->name }}">
                    @if ($errors->has('groupTitle'))
                        <span class="help-block">
                        <strong>{{ $errors->first('groupTitle') }}</strong>
                        </span>
                    @endif
                </div>
                {{ csrf_field() }}
                <button type="submit" class="btn btn-success btn-lg btn-block" name="opration" value="save"><i class="glyphicon glyphicon-saved"></i> @lang('group.forms.edit.btn_send') </button>
                
            </div>
        </div>
    </form>
    <div class="col-md-2">
        <div class="content-box-header">
            <div class="panel-title">@lang('group.forms.edit.shift_box_title')</div>
        </div>
        <div class="content-box-large box-with-header">
            <form class="form-horizontal" role="form" method="POST" action="">
                <select class="form-control" id="transferTo" name="transferTo" >
                @foreach($listAccs as $key => $acc)
                    <option value="{{ $acc->id }}">{{ $acc->name }}</option>
                @endforeach
                </select>
                <p></p>
                {{ csrf_field() }}
                <button type="submit" class="btn btn-success btn-lg btn-block" name="opration" value="transfer"><i class="glyphicon glyphicon-transfer"></i> @lang('group.forms.edit.btn_shift') </button>
            </form>
        </div>
    </div>
    </div>
</div>

@endsection