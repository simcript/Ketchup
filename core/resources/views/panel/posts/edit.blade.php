@extends('layouts.panel')

@section('sidbaractive_post','current')
@section('content_panel')
<link href="/build/tags/css/bootstrap-tags.css" rel="stylesheet">

<?php //dd($post->title) 
?>


<div class="col-md-10">
    <div class="row">
    <form class="form-horizontal" role="form" method="POST" action="">
    
        <div class="col-md-9">
            <div class="content-box-header">
                <div class="panel-title"><?= trans('newpost.post_box.edit_title', ['create_time' => $post->created_at, 'up_time' =>  $post->updated_at ]); ?></div>

                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-repeat"></i></a>
                </div>
            </div>
            <div class="content-box-large box-with-header">
                
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('postTitle') ? ' has-error' : '' }}">
                        <input id="postTitle" type="text" class="form-control" name="postTitle" placeholder="@lang('newpost.post_box.textbox_title')" value="{{ $post->title }}">
                        @if ($errors->has('postTitle'))
                            <span class="help-block">
                            <strong>{{ $errors->first('postTitle') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('postContent') ? ' has-error' : '' }}">
                        <textarea rows="20" id="tinymce_basic" name="postContent">{{ $post->body }}</textarea>
                        @if ($errors->has('postContent'))
                            <span class="help-block">
                            <strong>{{ $errors->first('postContent') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('postTags') ? ' has-error' : '' }}">
                        <?php
                            $tag = '';
                            foreach ($post->tagName as $tags) {
                                $tag .= $tags . '+';
                            } 
                            $tag = ((session('uRule')[14]) || (session('mRule')[11])) ? substr($tag, 0,strlen($tag)-1) : ''; 
                        ?>
                            <input id="postTags" type="text" class="form-control" name="postTags" placeholder="@lang('newpost.post_box.textbox_tags')" value="{{ $tag }}">
                        @if ($errors->has('postTags'))
                            <span class="help-block">
                            <strong>{{ $errors->first('postTags') }}</strong>
                            </span>
                        @endif
                    </div>
            </div>
        
        </div>

        <div class="col-md-3">
            <div class="content-box-header">
                <div class="panel-title">@lang('newpost.save_box.title')</div>
            </div>
            <div class="content-box-large box-with-header">

                <label for="select-statusComment">@lang("newpost.save_box.comment_status_list_title")</label>
                <select name="statusComment" id="select-statusComment" class="form-control custom-scroll">
                @for($i=0;$i<10;$i++)
                    @if(strlen(trans("newpost.save_box.comment_status_list.$i")) > 0)
                        <option @if($post->comments_status == $i) selected="selected" @endif value="{{ $i }}">@lang("newpost.save_box.comment_status_list.$i")</option>
                    @endif
                @endfor
                </select>
                <br>
                <label for="select-statusPost">@lang("newpost.save_box.status_list_title")</label>
                <select name="statusPost" id="select-statusPost" class="form-control custom-scroll">
                @for($i=0;$i<10;$i++)
                    @if(strlen(trans("newpost.save_box.status_list.$i")) > 0)
                        <option @if($post->status == $i) selected="selected" @endif value="{{ $i }}">@lang("newpost.save_box.status_list.$i")</option>
                    @endif
                @endfor
                </select>
                </br>

                <button type="submit" class="btn btn-success btn-lg btn-block" name="opration" value="publish"><i class="glyphicon glyphicon-saved"></i> @lang('newpost.save_box.btn_send') </button>
            </div>
        </div>

        <div class="col-md-3">
            <div class="content-box-header">
                <div class="panel-title">@lang('newpost.category_box.title')</div>
                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                </div>
            </div>
            <div class="content-box-large box-with-header">
                <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                    <select name="category[]" style="height:240px;" multiple="multiple" id="multiselect-1" class="form-control custom-scroll">
                        @foreach($allCategories as $key => $content)
                            <option @foreach($post->catIds as $id)
                                @if($content['id'] === $id) selected="selected" @endif 
                            @endforeach
                            value="{{ $content['id'] }}">{{ $content['name'] }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('category'))
                        <span class="help-block">
                        <strong>{{ $errors->first('category') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </form>

    </div>
</div>


    <script type="text/javascript" src="/build/tinymce/js/tinymce/tinymce.min.js"></script>
    <script src="/build/css/js/editors.js"></script>

@endsection