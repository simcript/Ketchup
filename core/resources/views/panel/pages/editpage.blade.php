@extends('layouts.panel')

@section('sidbaractive_page','current')
@section('content_panel')
<?php
// dd($errors); 
?>
<link href="/build/tags/css/bootstrap-tags.css" rel="stylesheet">

<div class="col-md-10">
    <div class="row">
    <form class="form-horizontal" role="form" method="POST" action="">
        <div class="col-md-9">
            <div class="content-box-header">
                <div class="panel-title"><?= trans('page.forms.edit.main_box_title', ['create_time' => $post->created_at, 'up_time' =>  $post->updated_at ]); ?></div>

                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-repeat"></i></a>
                </div>
            </div>
            <div class="content-box-large box-with-header">
                
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('pageTitle') ? ' has-error' : '' }}">
                        <input id="pageTitle" type="text" class="form-control" name="pageTitle" value="{{ $post->title }}">
                        @if ($errors->has('pageTitle'))
                            <span class="help-block">
                            <strong>{{ $errors->first('pageTitle') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('pageUrl') ? ' has-error' : '' }}">
                        <input id="pageUrl" type="text" class="form-control" name="pageUrl" value="{{ $post->url }}">
                        @if ($errors->has('pageUrl'))
                            <span class="help-block">
                            <strong>{{ $errors->first('pageUrl') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('pageContent') ? ' has-error' : '' }}">
                        <textarea rows="20" id="tinymce_basic" name="pageContent">{{ $post->body }}</textarea>
                        @if ($errors->has('pageContent'))
                            <span class="help-block">
                            <strong>{{ $errors->first('pageContent') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('pageTags') ? ' has-error' : '' }}">
                        <?php 
                            $tag = '';
                            foreach ($post->tagName as $tags) {
                                $tag .= $tags . '+';
                            } 
                            $tag = ((session('uRule')[14]) || (session('mRule')[11])) ? substr($tag, 0,strlen($tag)-1) : ''; 
                        ?>
                        <input id="pageTags" type="text" class="form-control" name="pageTags" placeholder="@lang('newpost.post_box.textbox_tags')" value="{{ $tag }}">
                        @if ($errors->has('pageTags'))
                            <span class="help-block">
                            <strong>{{ $errors->first('pageTags') }}</strong>
                            </span>
                        @endif
                    </div>
                
            </div>
        
        </div>
        <div class="col-md-3">
            <div class="content-box-header">
                <div class="panel-title">@lang('page.forms.new.save_box_title')</div>
            </div>
            <div class="content-box-large box-with-header">

                <label for="select-statusComment">@lang("page.forms.new.comment_status_list_title")</label>
                <select name="statusComment" id="select-statusComment" class="form-control custom-scroll">
                @for($i=0;$i<10;$i++)
                    @if(strlen(trans("page.forms.new.comment_status_list.$i")) > 0)
                        <option @if($post->comments_status == $i) selected="selected" @endif value="{{ $i }}">@lang("page.forms.new.comment_status_list.$i")</option>
                    @endif
                @endfor
                </select>
                <br>

                <label for="select-statuspage">@lang("page.forms.new.status_list_title")</label>
                <select name="statusPage" id="select-statuspage" class="form-control custom-scroll">
                @for($i=0;$i<10;$i++)
                    @if(strlen(trans("page.forms.new.status_list.$i")) > 0)
                        <option @if($post->status == $i) selected="selected" @endif value="{{ $i }}">@lang("page.forms.new.status_list.$i")</option>
                    @endif
                @endfor
                </select>
                </br>
                
                <button type="submit" class="btn btn-success btn-lg btn-block" name="opration" value="publish"><i class="glyphicon glyphicon-saved"></i> @lang('page.forms.new.btn_send') </button>
            </div>
        </div>
    </form>

    </div>
</div>


    <script type="text/javascript" src="/build/tinymce/js/tinymce/tinymce.min.js"></script>
    <script src="/build/css/js/editors.js"></script>


@endsection