@extends('layouts.panel')

@section('sidbaractive_setting','current')
@section('content_panel')
<div class="col-md-10">
    <form class="form-horizontal" role="form" method="POST" action="">

        <div class="col-md-9">
            <div class="content-box-header">
                <div class="panel-title">@lang('settings.form.site_box.box_title')</div>

                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-repeat"></i></a>
                </div>
            </div>
            <div class="content-box-large box-with-header">

                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('siteTitle') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <div class="input-group-addon">@lang('settings.form.site_box.title')</div>
                            <input id="siteTitle" type="text" class="form-control" name="siteTitle" value="{{ $sett->title or old('siteTitle') }}">
                        </div>
                        @if ($errors->has('siteTitle'))
                            <span class="help-block">
                            <strong>{{ $errors->first('siteTitle') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('siteName') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <div class="input-group-addon">@lang('settings.form.site_box.name')</div>
                            <input id="siteName" type="text" class="form-control" name="siteName" value="{{ $sett->name or old('siteName') }}">
                        </div>
                        @if ($errors->has('siteName'))
                            <span class="help-block">
                            <strong>{{ $errors->first('siteName') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('siteDesc') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <div class="input-group-addon">@lang('settings.form.site_box.description')</div>
                            <textarea class="form-control" rows="3" id="siteDesc" name="siteDesc">{{ $sett->description or old('siteDesc') }}</textarea>
                        </div>
                        @if ($errors->has('siteDesc'))
                            <span class="help-block">
                            <strong>{{ $errors->first('siteDesc') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('siteTags') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <div class="input-group-addon">@lang('settings.form.site_box.tags')</div>
                            <input id="siteTags" type="text" class="form-control" name="siteTags" value="{{ $sett->tags or old('siteTags') }}">
                        </div>
                        @if ($errors->has('siteTags'))
                            <span class="help-block">
                            <strong>{{ $errors->first('siteTags') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('siteAvail') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <div class="input-group-addon">@lang('settings.form.site_box.availability.label')</div>
                            <select name="siteAvail" id="select-siteAvail" class="form-control custom-scroll">
                                <option value="all" @if($sett->availability == 'all') selected @endif >@lang('settings.form.site_box.availability.forall')</option>
                                <option value="users" @if($sett->availability == 'users') selected @endif >@lang('settings.form.site_box.availability.users')</option>
                                <option value="nobody" @if($sett->availability == 'nobody') selected @endif >@lang('settings.form.site_box.availability.nobody')</option>
                            </select>
                        </div>
                        @if ($errors->has('siteAvail'))
                            <span class="help-block">
                            <strong>{{ $errors->first('siteAvail') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('siteNumPost') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <div class="input-group-addon">@lang('settings.form.site_box.no_posts')</div>
                            <input id="siteNumPost" type="number" min="1" class="form-control" name="siteNumPost" value="{{ $sett->total_post or old('siteNumPost') }}">
                        </div>
                        @if ($errors->has('siteNumPost'))
                            <span class="help-block">
                            <strong>{{ $errors->first('siteNumPost') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('siteAbstract') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <div class="input-group-addon">@lang('settings.form.site_box.abstracts')</div>
                            <input id="siteAbstract" type="number" min="30" class="form-control" name="siteAbstract" value="{{ $sett->abstracts or old('siteAbstract') }}">
                        </div>
                        @if ($errors->has('siteAbstract'))
                            <span class="help-block">
                            <strong>{{ $errors->first('siteAbstract') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('siteFooter') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <div class="input-group-addon">@lang('settings.form.site_box.footer')</div>
                            <input id="siteFooter" type="text" class="form-control" name="siteFooter" value="{{ $sett->footer or old('siteFooter') }}">
                        </div>
                        @if ($errors->has('siteFooter'))
                            <span class="help-block">
                            <strong>{{ $errors->first('siteFooter') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('siteRtL') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <div class="input-group-addon">@lang('settings.form.site_box.rtl')</div>
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default @if($sett->rtl) active @endif" >
                                    <input type="radio" name="siteRtL" id="optionsiteRtL" value="1" @if($sett->rtl) checked @endif>  @lang('settings.form.input_option.yes')
                                </label>
                                <label class="btn btn-default @if(!$sett->rtl) active @endif">
                                    <input type="radio" name="siteRtL" id="optionsiteRtL" value="0" @if(!$sett->rtl) checked @endif >  @lang('settings.form.input_option.no')
                                </label>
                            </div>
                        </div>
                        @if ($errors->has('siteRtL'))
                            <span class="help-block">
                            <strong>{{ $errors->first('siteRtL') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('siteNotic') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <div class="input-group-addon">@lang('settings.form.site_box.notic')</div>
                            <textarea class="form-control" rows="7" id="siteNotic" name="siteNotic">{{ $sett->site_notic or old('siteNotic') }}</textarea>
                        </div>
                        @if ($errors->has('siteNotic'))
                            <span class="help-block">
                            <strong>{{ $errors->first('siteNotic') }}</strong>
                            </span>
                        @endif
                    </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="content-box-header">
                <div class="panel-title">@lang('settings.form.save_box.box_title')</div>
            </div>
            <div class="content-box-large box-with-header">
                
                <button type="submit" class="btn btn-success btn-lg btn-block" name="opration" value="SaveSettings"><i class="glyphicon glyphicon-saved"></i>@lang('settings.form.save_box.btn_send')</button>
            </div>
        </div>

        <div class="col-md-9">
            <div class="content-box-header">
                <div class="panel-title">@lang('settings.form.panel_box.box_title')</div>

                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-repeat"></i></a>
                </div>
            </div>
            <div class="content-box-large box-with-header">

                    <div class="form-group{{ $errors->has('panelNotic') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <div class="input-group-addon">@lang('settings.form.panel_box.notic')</div>
                            <textarea class="form-control" rows="7" id="panelNotic" name="panelNotic">{{ $sett->panel_notic or old('panelNotic') }}</textarea>
                        </div>
                        @if ($errors->has('panelNotic'))
                            <span class="help-block">
                            <strong>{{ $errors->first('panelNotic') }}</strong>
                            </span>
                        @endif
                    </div>
            </div>
        </div>

        <div class="col-md-9">
            <div class="content-box-header">
                <div class="panel-title">@lang('settings.form.auth_box.box_title')</div>

                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-repeat"></i></a>
                </div>
            </div>
            <div class="content-box-large box-with-header">

                    <div class="form-group{{ $errors->has('authRegister') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <div class="input-group-addon">@lang('settings.form.auth_box.register')</div>
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default @if($sett->register) active @endif">
                                    <input type="radio" name="authRegister" id="optionauthRegister" value="1" @if($sett->register) checked @endif>  @lang('settings.form.input_option.yes')
                                </label>
                                <label class="btn btn-default @if(!$sett->register) active @endif">
                                    <input type="radio" name="authRegister" id="optionauthRegister" value="0" @if(!$sett->register) checked @endif >  @lang('settings.form.input_option.no')
                                </label>
                            </div>
                        </div>
                        @if ($errors->has('authRegister'))
                            <span class="help-block">
                            <strong>{{ $errors->first('authRegister') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('defaultUserGroups') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <div class="input-group-addon">@lang('user.forms.new.selectbox_groups')</div>
                            <select name="defaultUserGroups" id="select-defaultUserGroups" class="form-control custom-scroll">
                                @if($sett == null) <option value="0">@lang('user.forms.new.selectbox_default')</option> @endif
                                @foreach($groups as $g)
                                    <option @if(($sett != null) && ($sett->default_rule ==  $g->id)) selected @endif value="{{ $g->id }}">{{ $g->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        @if ($errors->has('defaultUserGroups'))
                            <span class="help-block">
                            <strong>{{ $errors->first('defaultUserGroups') }}</strong>
                            </span>
                        @endif
                    </div>
            </div>
        </div>

        <div class="col-md-9">
            <div class="content-box-header">
                <div class="panel-title">@lang('settings.form.file_box.box_title')</div>

                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-repeat"></i></a>
                </div>
            </div>
            <div class="content-box-large box-with-header">

                    <div class="form-group{{ $errors->has('fileMime') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <div class="input-group-addon">@lang('settings.form.file_box.mime')</div>
                            <input id="fileMime" type="text" class="form-control" name="fileMime" value="{{ $sett->mime or old('fileMime') }}">
                        </div>
                        @if ($errors->has('fileMime'))
                            <span class="help-block">
                            <strong>{{ $errors->first('fileMime') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('fileMaxsize') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <div class="input-group-addon">@lang('settings.form.file_box.max_size.label')</div>
                            <input id="fileMaxsize" type="number" min="0" class="form-control" name="fileMaxsize" value="{{ $sett->upsize or old('fileMaxsize') }}">
                            <span class="input-group-addon">@lang('settings.form.file_box.max_size.in')</span>
                        </div>
                        @if ($errors->has('fileMaxsize'))
                            <span class="help-block">
                            <strong>{{ $errors->first('fileMaxsize') }}</strong>
                            </span>
                        @endif
                    </div>
            </div>
        </div>

    </form>
</div>
@endsection