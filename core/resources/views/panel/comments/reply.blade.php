@extends('layouts.panel')

@section('sidbaractive_comment','current')
@section('content_panel')
<link href="/build/tags/css/bootstrap-tags.css" rel="stylesheet">
<?php //dd($errors); ?>
<div class="col-md-10">
    <div class="row">
    <form class="form-horizontal" role="form" method="POST" action="">
        <div class="col-md-9">
            <div class="content-box-header">
                <div class="panel-title">@lang("comment.forms.edit.main_box_title")</div>

                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-repeat"></i></a>
                </div>
            </div>
            <div class="content-box-large box-with-header">
                
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('commName') ? ' has-error' : '' }}">
                        <label for="commName">@lang("comment.forms.edit.name")</label>
                        <input id="commName" type="text" class="form-control" name="commName" value="{{ $comm->name }}">
                        @if ($errors->has('commName'))
                            <span class="help-block">
                            <strong>{{ $errors->first('commName') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('commEmail') ? ' has-error' : '' }}">
                        <label for="commEmail">@lang("comment.forms.edit.email")</label>
                        <input id="commEmail" type="text" class="form-control" name="commEmail" value="{{ $comm->email }}">
                        @if ($errors->has('commEmail'))
                            <span class="help-block">
                            <strong>{{ $errors->first('commEmail') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('commBody') ? ' has-error' : '' }}">
                        <label for="commBody">@lang("comment.forms.edit.contentComment")</label>
                        <textarea class="form-control" rows="5" id="commBody" name="commBody">{{ $comm->body }}</textarea>
                        @if ($errors->has('commBody'))
                            <span class="help-block">
                            <strong>{{ $errors->first('commBody') }}</strong>
                            </span>
                        @endif
                    </div>
                    
                    <div class="form-group{{ $errors->has('replyBody') ? ' has-error' : '' }}">
                        <label>@lang("comment.forms.edit.replyContent")</label>
                        <textarea rows="10" id="tinymce_basic" name="replyBody"></textarea>
                        @if ($errors->has('replyBody'))
                            <span class="help-block">
                            <strong>{{ $errors->first('replyBody') }}</strong>
                            </span>
                        @endif
                    </div>

            </div>
        
        </div>
        <div class="col-md-3">
            <div class="content-box-header">
                <div class="panel-title">@lang('comment.forms.edit.save_box_title')</div>
            </div>
            <div class="content-box-large box-with-header">
                <label for="select-statuspage">@lang("comment.forms.edit.status_list_title")</label>
                <select name="statusComm" id="select-statuspage" class="form-control custom-scroll">
                @for($i=0;$i<10;$i++)
                    @if(strlen(trans("comment.forms.edit.status_list.$i")) > 0)
                        <option @if($comm->status == $i) selected="selected" @endif value="{{ $i }}">@lang("comment.forms.edit.status_list.$i")</option>
                    @endif
                @endfor
                </select>
                </br>
                <button type="submit" class="btn btn-success btn-lg btn-block" name="opration" value="send"><i class="glyphicon glyphicon-saved"></i> @lang('comment.forms.edit.btn_send') </button>
            </div>
        </div>
    </form>
        @if($countReply > 0)
            <div class="col-md-3">
                <div class="content-box-header">
                    <div class="panel-title">@lang('comment.forms.edit.replies_box_title')</div>
                </div>
                <div class="content-box-large box-with-header">
                    @if($comm->status == 3)
                        <p class="text-center">@lang('comment.forms.edit.reply_new')</p>
                    @endif
                    <strong><?= trans('comment.forms.edit.reply_count', ['count' => $countReply, 'type' =>  ($countReply > 1) ? 'replies' : 'reply' ]); ?></strong>
                    </br>
                    </br>
                    <a class="btn btn-primary btn-lg btn-block" name="showReply" href="{{ $comm->id }}/all"><i class="glyphicon glyphicon-eye-open"></i> @lang('comment.forms.edit.btn_replies') </a>
                </div>
            </div>
        @endif
        <div class="col-md-3">
            <div class="content-box-header">
                <div class="panel-title">@lang('comment.forms.edit.details_box_title')</div>
            </div>
            <div class="content-box-large box-with-header">
                <p>@lang('comment.forms.edit.details_created') {{ $comm->created_at }}</p>
                <p>@lang('comment.forms.edit.details_updated') {{ $comm->updated_at }}</p>
                <p><?= trans('comment.forms.edit.details_post', ['link' => '/p/' . $comm->post_id]); ?></p>
                <p><?= ($comm->parent != null) ? trans('comment.forms.edit.details_parent', ['link' => $comm->parent]) : ''; ?></p>
                <p><?= ($comm->csrf != null) ? trans('comment.forms.edit.details_user', ['link' => '/panel/statistic/?id=' . $comm->csrf .'&d=' . $comm->created_at]) : ''; ?></p>
            </div>
        </div>
    </div>
</div>


    <script type="text/javascript" src="/build/tinymce/js/tinymce/tinymce.min.js"></script>
    <script src="/build/css/js/editors.js"></script>


@endsection