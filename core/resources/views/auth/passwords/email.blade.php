@extends('layouts.auth')

@section('content_auth')
<div class="container">

    <div class="page-content container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-wrapper">
                    <div class="box">
                        <div class="content-wrap">
                            <h6>Reset Password</h6>
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <input id="email" type="email" class="form-control" name="email" placeholder="E-mail address"  value="{{ old('email') }}">

                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary"> Send Password Reset Link </button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="already">
                        <a href="{{ url('/register') }}">Register</a>
                        <p>Or</p>
                        <a href="{{ url('/login') }}">Log in</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
