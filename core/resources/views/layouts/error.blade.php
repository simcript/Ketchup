<!DOCTYPE html>
<html>
    <head>
        <title>@yield('title')</title>
        <meta http-equiv="Designer" content="AliA_MehR | alia_mehr@yahoo.com">
        <meta name="Generator" content="Sublime Text">
        <meta name="copyright" content="Built-in time 2017-03-04 11:27:50 | home : https://gitlab.com/alia_mehr" />
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #292C2E;
                display: table;
                font-weight: 100;
                font-family: 'Arial';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
                color: #B0BEC5;
                font-family: 'Lato';
                
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">@yield('title')</div>
                <div class="body">@yield('body')</div>
            </div>
        </div>
    </body>
</html>