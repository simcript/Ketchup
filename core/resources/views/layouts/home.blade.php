<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

            <title>@yield('title')</title>
            <meta name="description" content=" @yield('description') " />
            <meta name="keywords" content=" @yield('tags') " />
            <meta name="author" content=" @yield('author') " />

            <meta http-equiv="Designer" content="AliA_MehR | alia_mehr@yahoo.com">
            <meta name="Generator" content="Sublime Text">
            <meta name="copyright" content="Built-in time 2017-03-04 11:27:50 | home : https://gitlab.com/alia_mehr" />
            <meta name="robots" content="index, follow" />

    <link rel="stylesheet" href="{{ elixir('css/style.css') }}" >
    <style>
        body{
            background-color: #f1f1f1;
        }
        #mainpage{
            background-color: #ffffff;
            margin-bottom: 200px;
        }

        /* Set black background color, white text and some padding */
        footer {
            background-color: #555;
            color: white;
            padding: 15px;
        }

        /* On small screens, set height to 'auto' for sidenav and grid */
        @media screen and (max-width: 767px) {
            .sidenav {
                height: auto;
                padding: 15px;
            }
            .row.content {height: auto;} 
        }
    </style>
</head>
<body>
    <div class="container-fluid">
        <div class="row content">
            <div class="col-sm-3 sidenav">
                <a href="/" style="color:#000;text-decoration:none;"><h3>@yield('siteName')</h3></a>
                <h5>@yield('siteDesc')</h5>
                <form role="form" method="GET" action="{{ url('/') }}">
                    <div class="input-group">
                        <input name="search" type="text" class="form-control" placeholder="Search Blog..">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                        </span>
                    </div>
                </form><br>
                <ul class="nav nav-pills nav-stacked">
                    @yield('links')
                </ul>
                
            </div>

            <div id="mainpage" class="col-sm-9">
                @yield('content')
            </div>
        </div>
    </div>

    <footer class="container-fluid navbar-fixed-bottom">
        <p class="text-center">@yield('footer')</p>
    </footer>
    <script src="{{ elixir('js/script.js') }}"></script>
</body>
</html>

