<?php

return [
	'links' => [
		'sections' => [
			'add' => 'Add new link',
			'list' => 'List of Links'
		],
		'form' => [
			'title' => 'Title',
			'description' => 'Description',
			'address' => 'Link Address',
			'save' => 'Add Link',
		],
		'tableColumns' => [
	        'rownumber' => '#',
	        'title' => 'Title',
	        'content' => 'Description',
	        'address' => 'Address',
	        'date' => 'Date',
	        'oprations' => '&nbsp;'
		]
	]
];