<?php

return [
    'table' => [
    	'rownumber' => '#',
        'name' => 'Name',
        'email' => 'Email',
        'joined' => 'Join Date',
        'oprations' => '&nbsp;'
    ],
    'forms' => [
        'new' => [
            'account_box_title' => 'New User : <small>Account information</small>',
        	'profile_box_title' => 'New User : <small>Profile information</small>',
            'textbox_username' => 'Username',
        	'textbox_email' => 'Email',
            'textbox_pass' => 'Password',
            'selectbox_groups' => 'Group user',
            'selectbox_default' => 'select one...',
            'textbox_name' => 'Name',
            'textbox_pic' => 'Picture url',
            'textbox_bd' => 'Brith date',
            'textbox_infomail' => 'Email',
            'textbox_phone' => 'Phone No.',
            'textbox_address' => 'Address',
            'textbox_education' => 'Education',
            'textbox_job' => 'Job',
            'textbox_favorites' => 'Favorites',
            'textbox_about' => 'About',
            'save_box_title' => 'Create User',
	        'btn_send' => 'Save'
        ],
        'edit' => [
            'account_box_title' => ':username : <small>Account information</small>',
            'profile_box_title' => ':username : <small>Profile information</small>',
            'textbox_pass' => 'for change type new password',
            'save_box_info' => ':username at <mark>:create_time</mark> created and last Update at <mark>:up_time</mark>',
            'save_box_title' => 'Save',
        ]
    ],


];