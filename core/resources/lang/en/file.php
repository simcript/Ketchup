<?php

return [
    'table' => [
    	'rownumber' => '#',
        'title' => 'File name',
        'mime' => 'Type',
        'totalDl' => 'Downloads',
        'uptime' => 'Upload Time',
        'oprations' => '&nbsp;'
    ],
    'forms' => [
        'new' => [
        	'main_box_title' => 'File Details',
            'textbox_name' => 'Name',
        	'textbox_description' => 'Description',
            'textbox_pass' => 'Password',
	        'textbox_tags' => 'Tags',
            'filebox_file' => 'Choise File',
            'save_box_title' => 'Upload File',
	        'btn_send' => 'Upload'
        ]
    ],


];