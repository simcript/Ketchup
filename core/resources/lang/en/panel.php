<?php

return [
    'title' => [
        'site_title' => 'Panel',
        'brand' => 'Admin Panel',
        'nav' => [
            'dashboard' => 'Dashboard',
            'site' => 'View Site',
            'profile' => 'View Profile',
            'exit' => 'Exit'
        ]
    ],
    'navbar' => [
        'dashboard' => 'Dashboard',
        'posts' => [
            'list'=>'Posts',
            'all'=>'All Posts',
            'new'=>'New',
            'trash'=>'Trash Bin'
        ],
        'pages' => [
            'list'=>'Pages',
            'all'=>'All Pages',
            'new'=>'New',
            'trash'=>'Trash Bin'
        ],
        'comments' => [
            'list'=>'Comments',
            'all'=>'All Comments',
            'spam'=>'Spam'
        ],
        'seo' => [
            'list'=>'SEO',
            'cats'=>'Categories',
            'tags'=>'Tags',
            'new'=>'New'
        ],
        'fms' => [
            'list'=>'Files',
            'all'=>'All',
            'new'=>'New',
            'trash'=>'Trash Bin'
        ],
        'users' => [
            'list'=>'Users',
            'active'=>'Active Users',
            'deactive'=>'Deactive Users',
            'new'=>'New',
            'edit'=>'Edit'
        ],
        'groups' => [
            'list'=>'Groups',
            'all'=>'All',
            'new'=>'New'
        ],
        'settings' => 'Settings',
        'tools' => [
            'list'=>'Tools',
            'link'=>'Links'
        ],
        'exit' => 'Exit'
     ],
     'adminOption' => [
        'me' => 'Me',
        'all' => 'All',
     ],
    'index' => [
        'welcome' => 'Welcome :user',
     ],
     'footer_text' => 'Powered by <a href="https://gitlab.com/alia_mehr/Ketchup">Ketchup</a>',

];