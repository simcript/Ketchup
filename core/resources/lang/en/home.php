<?php

return [
    'index' => [
    	'notic_place' => 'RECENT POSTS',
        'pager' => [
            'previous' => 'Older',
            'next' => 'Next'
        ],
        'comment_form' => 'Leave a Comment:',
        'comment_list' => 'Comment(s)',
        'comment_reply' => 'Reply',
        'comment_reply_title' => 'Reply to ',
        
    ],
    'errors'=> [
        'nobody' => 'Site is unavailable',
        'user_posts' => 'This post only for users',
        '404' => [
            'title' => '404',
            'body' => 'Web page not found.',
        ],
        'comment' => [
            'only_user' => 'Please login to site for send comment.',
            'nobody' => 'There is no possibility to send a comment',
        ]
    ],
    'forms'=> [
        'comment' => [
            'textbox_name' => 'Name',
            'textbox_email' => 'Email',
            'textbox_body' => 'Comment',
            'button_send' => 'Submit',
        ]
    ]
];