<?php

return [
    'tableColumns' => [
        'rownumber' => '#',
        'title' => 'Title',
        'content' => 'Body',
        'date' => 'Date',
        'oprations' => '&nbsp;'
    ]

];