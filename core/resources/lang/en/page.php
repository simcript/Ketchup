<?php

return [
    'table' => [
    	'rownumber' => '#',
        'title' => 'Title',
        'content' => 'Body',
        'date' => 'Date',
        'oprations' => '&nbsp;'
    ],
    'forms' => [
        'new' => [
        	'main_box_title' => 'New Page',
            'textbox_title' => 'Page Title',
        	'textbox_url' => 'Page Url',
            'textbox_content' => 'Explain more....',
	        'textbox_tags' => 'Tags',
            'save_box_title' => 'Create Page',
            'comment_status_list_title' => 'I accept Comments :',
            'comment_status_list' => [
                '0' => 'anyone',
                '1' => 'only members',
                '2' => 'Nobody',
                '3' => '',
                '4' => '',
                '5' => '',
                '6' => '',
                '7' => '',
                '8' => '',
                '9' => ''
            ],
            'status_list_title' => 'This Page',
            'status_list' => [
                '0' => 'Show',
                '1' => 'Special Members',
                '2' => 'Draft',
                '3' => 'For Review',
                '4' => '',
                '5' => '',
                '6' => '',
                '7' => '',
                '8' => '',
                '9' => ''
            ],
	        'btn_send' => 'Save'
        ],
        'edit' => [
            'main_box_title' => 'Edit Page : <small> at <mark>:create_time</mark> created and last Update at <mark>:up_time</mark></small>',
            'save_box_title' => 'Save',
            'btn_save_cat' => 'Save Change',
            'btn_save_tag' => 'Save Change',
        ]
    ],


];