<?php

return [
    'table' => [
    	'rownumber' => '#',
        'title' => 'Access Name',
        'date' => 'Create Date',
        'update' => 'Update',
        'oprations' => '&nbsp;'
    ], 
    'forms' => [
        'new' => [
            'user_box_title' => 'New Group : User Access',
        	'manage_box_title' => 'New Group : Management Access',
            'save_box_title' => 'Create Group',
            'name_group' => 'Group Name',
            'btn_send' => 'Save',
            'btn_allow' => 'Yes',
            'btn_ban' => 'No',
            'user_box_list' => [
                '0' => 'Create new post',
                '1' => 'Edit post',
                '2' => 'Delete post',
                '3' => 'View post',
                '4' => 'Create new page',
                '5' => 'Edit page',
                '6' => 'Delete page',
                '7' => 'View page',
                '8' => 'Send comment',
                '9' => 'Edit comment',
                '10' => 'Delete comment',
                '11' => 'View comment',
                '12' => 'Create SEO',
                '13' => 'Edit SEO',
                '14' => 'View SEO',
                '15' => 'Upload File',
                '16' => 'View File',
                '17' => 'Delete File',
                '18' => 'Edit File'
            ],
            'manage_box_list' => [
                '0' => 'Edit user\'s posts',
                '1' => 'Delete user\'s post',
                '2' => 'View user\'s post',
                '3' => 'Edit user\'s page',
                '4' => 'Delete user\'s page',
                '5' => 'View user\'s page',
                '6' => 'Edit user\'s comment',
                '7' => 'Delete user\'s comment',
                '8' => 'View user\'s comment',
                '9' => 'Create SEO',
                '10' => 'Edit SEO',
                '11' => 'View SEO',
                '12' => 'Delete SEO',
                '13' => 'Add user',
                '14' => 'View user\'s informations',
                '15' => 'Edit user\'s informations',
                '16' => 'Delete user',
                '17' => 'Active/Inactive user\'s Account',
                '18' => 'Publication Notifications',
                '19' => 'Create new Group',
                '20' => 'Edit Group',
                '21' => 'View Group',
                '22' => 'Delete Group',
                '23' => 'View user\'s File',
                '24' => 'Delete user\'s File',
                '25' => 'Edit user\'s File',
                '26' => 'Change Site Settings',
                '27' => 'Using Tools'
            ]
        ],
        'edit' => [
            'user_box_title' => 'Edit Group : User Access',
            'manage_box_title' => 'Edit Group : Management Access',
            'save_box_title' => 'Save Change',
            'shift_box_title' => 'Transfer to',
            'btn_shift' => 'Start',
            'btn_send' => 'Save'
        ]
    ],


];