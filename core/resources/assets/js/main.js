function myTimer () {
	var tempTime = document.getElementById('mainTimer').value;
	var second = (tempTime > 0 ) ? tempTime : 0;
	var minute = 0;             
	var hour = 0;             
	var day = 0;             
	while (second > 59){
		minute++;
		second = second - 60;
		if(second < 0){
			second = second * (-1);
		}   
	}
	while (minute > 59){
		hour++;
		minute = minute - 60;
		if(minute < 0){
			minute = minute * (-1);
		}
	}
	while (hour > 23){
		day++;
		hour = hour - 24;
		if(hour<0){
			hour = hour * (-1);
		}
	}
	document.getElementById('mainTimer').value = tempTime-1;
	if (tempTime == 0) {
		clearInterval(timer);
		if (loc.length < 2) {
			window.location.reload();
		};
	};
	tempTime = ((day < 10) ? "0" + day : day) + ':' + ((hour < 10) ? "0" + hour : hour) + ':' + ((minute < 10) ? "0" + minute : minute) + ':' + ((second < 10) ? "0" + second : second);
	var result = document.getElementsByClassName('resulttime');
	for (var i = 0; i < result.length; i++) {
		result[i].innerHTML = tempTime;
	};
	
}
var loc = window.location.pathname;
var timer=setInterval(myTimer,1000);