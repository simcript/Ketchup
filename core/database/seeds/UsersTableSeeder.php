<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'alia_mehr@yahoo.com',
            'rule_id'=> '1',
            'password' => bcrypt('123456'),
        ]);

        DB::table('users')->insert([
            'name' => 'testuser',
            'email' => 'testuser@test.com',
            'rule_id'=> '2',
            'password' => bcrypt('123456'),
        ]);
    }
}
