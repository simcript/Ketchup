<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'Uncategorized',
            'description' => 'this category is a default categories'
        ]);

        for ($i=1; $i < 6 ; $i++) { 
	        DB::table('categories')->insert([
	            'name' => 'category_'.$i,
	            'description' => 'this category is a test categories about' . str_random(10*$i),
	        ]);
        }

        // tags for posts
        for ($p=50; $p < 72 ; $p++) {
            DB::table('category_post')->insert([
                'post_id' => $p,
                'category_id' => rand(1,6),
            ]);
            
        }
    }
}