<?php

use Illuminate\Database\Seeder;

class AccessesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('accesses')->insert([
        	'name' => 'Admin',
            'usage_rules' => 'eyJpdiI6Ijkrc3JqeDVwazQzb3JROElya3VZTGc9PSIsInZhbHVlIjoicWpEZ3dvaU0zcUpTeDdKTmVESmhhV0V0QTlmQk1oK0NTT056Yk9oSURcL0Z5bDVvNlRMXC9ReUI5d09RNityNW1KIiwibWFjIjoiYWRkYTcwYjk3MGFiZWJhZjdmNjM5MjNiZDZmYzY2ZTAzNDRhYWE0MDRkYTY0YTYyN2FkYzY4MmFkMzMwMWRkZiJ9',
            'manage_rules' => 'eyJpdiI6IlJPTXYrQXZFVHhuNFNiS09lZ0NxSmc9PSIsInZhbHVlIjoiY2JjSFdQWVRtVjBYTzlWZWFjd1FJM0pVR096QUR3aUpzVFpcL0NhWXpxVnJma0hSUnhrV2VTWnU2MzdiYk5hN0QxSk1NSEJRc1JsdVZHNGsrTEdwaU1FSGg0MVwvTUx0bG9YZ0RiWm03U0ZUST0iLCJtYWMiOiI5MDM5ZDlmMDQxNjU0MTI2OGU3ZjhmZTAxNDA3ZDlhMTk1ZmRkMzA0MzA1NTFmYTdjYWY2MDI1MDE5OTQ2MzU3In0'
        ]);

        DB::table('accesses')->insert([
        	'name' => 'member',
        	'usage_rules' => 'eyJpdiI6Ijkrc3JqeDVwazQzb3JROElya3VZTGc9PSIsInZhbHVlIjoicWpEZ3dvaU0zcUpTeDdKTmVESmhhV0V0QTlmQk1oK0NTT056Yk9oSURcL0Z5bDVvNlRMXC9ReUI5d09RNityNW1KIiwibWFjIjoiYWRkYTcwYjk3MGFiZWJhZjdmNjM5MjNiZDZmYzY2ZTAzNDRhYWE0MDRkYTY0YTYyN2FkYzY4MmFkMzMwMWRkZiJ9',
            'manage_rules' => 'eyJpdiI6InZnS1krZ1Q1SUJ4WmpaMU5NWXkyOEE9PSIsInZhbHVlIjoiaEtDQXVmckJMb054S2syTmM2MUdlcGJDeHpcL3l0aW1HZnlHZXZqcGplcnM5QkJSQnNjeWNvc3lCY0k1M29hMzlUQk8yM3doMmF0R1dmZ281eWVUVFNhanhQSTZLS2JPOHEyNjF5dUVmeGxvPSIsIm1hYyI6IjI5YzFkYTY3NWM4OGM5NTc5Y2ZiNGQyYTJmYTNkZGIzODM3YzFlNWNjYTg5ZGExY2EyNWIzNDk5Y2FlZjBmYTUifQ'
        ]);
    }
}
