<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$temp = "Lorem ipsum Velit minim nostrud ullamco non eu in mollit Ut consectetur occaecat aute voluptate Duis aliquip Excepteur non deserunt deserunt amet pariatur amet voluptate nisi officia id aute veniam consectetur laborum et minim dolore labore.Lorem ipsum Adipisicing aliquip laboris irure quis dolore laboris sunt cillum eu id amet ea ex fugiat Excepteur esse incididunt ullamco ea dolor sed culpa officia quis eiusmod est sed labore id anim in dolore fugiat dolor cupidatat cupidatat eiusmod deserunt sit.";
    	for ($i=0; $i < 39 ; $i++) { 
    		$tempRand = rand(0,10);
    		DB::table('tags')->insert([
	            'name' => str_random(rand(9,30)),
	            'description' => substr($temp,$tempRand, $tempRand + 10) . str_random(20),
	            'created_at' => "2017-03-".rand(0,2)."6 15:".rand(10,59).":53",
	            'updated_at' => "2017-03-".rand(0,2)."6 15:".rand(10,59).":53"
        	]);
    	}
// tags for posts
    	for ($p=50; $p < 72 ; $p++) { 
    		$startTemp = rand(18,49);
    		$endTemp = $startTemp + rand(1,6);
    		for ($t=$startTemp; $t < $endTemp ; $t++) {
	    		DB::table('post_tag')->insert([
		            'post_id' => $p,
		            'tag_id' => $t,
	        	]);
	    	}
    	}
    	

    }
}
