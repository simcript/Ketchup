<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('title',30)->nullable();
            $table->string('name',60);
            $table->string('description',510)->nullable();
            $table->string('tags',255)->nullable();
            $table->enum('availability',['nobody','users','all']);
            $table->tinyInteger('total_post')->unsigned();
            $table->smallInteger('abstracts')->unsigned();
            $table->string('footer',255);
            $table->boolean('rtl');
            $table->text('site_notic')->nullable();
            $table->text('panel_notic')->nullable();
            $table->boolean('register');
            $table->integer('default_rule')->unsigned();
            $table->text('mime');
            $table->string('upsize',30);
            $table->timestamps();
            $table->foreign('default_rule')->references('id')->on('accesses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('settings');
    }
}
